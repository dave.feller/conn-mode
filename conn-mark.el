;;; conn-mark.el --- Mark cursor for conn-mode -*- lexical-binding: t -*-
;;
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.
;;
;;; Commentary:
;;
;; Mark cursor for conn-mode.
;;
;;; Code:

(require 'compat)
(require 'conn-utils)
(require 'conn-dots)
(require 'conn-core)

(defface conn-mark-face
  '((default (:background "gray65")))
  "Face for mark."
  :group 'conn-mode)

(defcustom conn-mark-trail-commands
  '(next-line
    conn-back-to-indentation-or-beginning
    conn-end-of-line-or-next
    previous-line)
  "Commands which leave an ephemeral mark behind them."
  :type '(list symbol)
  :group 'conn-mode)

(defcustom conn-hide-mark-cursor-modes '((minibuffer-mode emacs-state)
                                          (eshell-mode emacs-state))
  "Alist of (MODE . STATES) in which to hide the mark cursor."
  :type '(list (list symbol))
  :group 'conn-mode)

(defvar-local conn--unpop-ring nil
  "Ring for marks popped with `pop-to-mark-command'.")

(defvar conn--global-unpop-ring nil
  "Ring for marks popped with `pop-global-mark'.")

(defvar conn--hide-mark-alist nil)

(defvar-local conn--ephemeral-mark nil)

(defvar conn--update-mark-cursor nil
  "`run-with-idle-timer' timer to update `mark' cursor.")

(defvar-local conn--hide-mark-cursor nil
  "Hide `mark' cursor when non-nil.")

(defvar-local conn--mark-cursor nil
  "`mark' cursor overlay.")
(put 'conn--mark-cursor 'permanent-local t)

(defvar conn--thing-movement-cmds
  '((sexp (forward-sexp) (backward-sexp))
    (buffer (beginning-of-buffer :discrete t) (end-of-buffer :discrete t))
    (word (forward-word) (backward-word))
    (line (forward-line) (conn-backward-line))
    (defun (beginning-of-defun) (end-of-defun))
    (paragraph (forward-paragraph) (backward-paragraph))
    (sentence (forward-sentence) (backward-sentence))
    (whitespace (forward-whitespace) (conn-backward-whitespace))
    (inner-line (conn-end-of-inner-line) (conn-beginning-of-inner-line))
    (dot (conn-next-dot) (conn-previous-dot))))

(defvar-local conn--thing-start-pt nil)

(defun conn--mark-cursor-p (ov)
  (eq (overlay-get ov 'type) 'conn--mark-cursor))

(defun conn--push-mark-ad (fn &rest args)
  (if (and conn--ephemeral-mark
           (not (use-region-p)))
      (let ((mark-ring nil))
        (apply fn args))
    (apply fn args))
  (setq conn--ephemeral-mark nil))

(defun conn--push-ephemeral-mark (&optional location msg activate)
  "Push a mark at LOCATION that will not be added to `mark-ring'.

For the meaning of MSG and ACTIVATE see `push-mark'."
  (let ((global-mark-ring nil))
    (push-mark location (not msg) activate))
  (setq conn--ephemeral-mark t))

(defun conn--refresh-cursor (&optional buffer)
  (with-current-buffer (or buffer (current-buffer))
    (when-let ((cursor (get conn-current-state :conn-cursor-type)))
      (setq cursor-type cursor))))

(defun conn--hide-mark-cursor-p (&optional buffer)
  (with-current-buffer (or buffer (current-buffer))
    (or (cdr (assoc (buffer-name buffer)
                    conn--hide-mark-alist
                    #'string-match-p))
        (conn--derived-mode-property :conn-hide-mark))))

(defun conn--update-mark-cursor-1 (win)
  (with-current-buffer (window-buffer win)
    (when-let ((mark-pos (mark t)))
      (cond
       ((or (eq t conn--hide-mark-cursor)
            (memq conn-current-state conn--hide-mark-cursor))
        (when conn--mark-cursor
          (delete-overlay conn--mark-cursor)
          (setq conn--mark-cursor nil)))
       ((null conn--mark-cursor)
        (setq conn--mark-cursor (make-overlay (mark t) (1+ (mark t)) nil t nil))
        (overlay-put conn--mark-cursor 'conn-overlay t)
        (overlay-put conn--mark-cursor 'face 'conn-mark-face)
        (overlay-put conn--mark-cursor 'type 'conn--mark-cursor)
        (overlay-put conn--mark-cursor 'priority conn-overlay-priority))
       (t
        (move-overlay conn--mark-cursor (mark t) (1+ (mark t)))
        (overlay-put conn--mark-cursor 'after-string
                     (when (and (= (mark-marker) (point-max))
                                (/= (point) (mark-marker)))
                       (propertize " " 'face 'conn-mark-face))))))))

(defun conn--update-mark-cursors ()
  (walk-windows #'conn--update-mark-cursor-1 nil 'visible))

(defun conn-hide-mark-cursor (mode-or-regexp &rest states)
  "Hide mark cursor in buffers matching MODE-OR-REGEXP.
MODE-OR-REGEXP is either a major mode or a regexp that
will be matched against each buffer's name.  STATES are
the states in which the mark cursor will be hidden, or if
no states are given the mark cursor will always be hidden."
  (cl-etypecase mode-or-regexp
    (symbol (put mode-or-regexp :conn-hide-mark (or states t)))
    (string (push (cons mode-or-regexp (or states t)) conn--hide-mark-alist))))

(defun conn-add-mark-trail-command (command)
  "Make COMMAND leave a mark trail."
  (put command :conn-mark-trail t))

(defun conn-add-thing-movement-command (thing command &rest properties)
  "Register a thing movement command for THING."
  (put command :conn-thing-movement-cmd thing)
  (when properties
    (put command :conn-thing-properties properties)))

(defun conn--mark-trail-pre-command-hook ()
  (cond
   (mark-active)
   ((conn--command-property :conn-mark-trail)
    (conn--push-ephemeral-mark (point)))
   ((conn--command-property :conn-thing-movement-cmd)
    (setq conn--thing-start-pt (point)))))
(put 'conn--mark-trail-pre-command-hook 'permanent-local-hook t)

(defun conn--thing-post-command-hook ()
  (with-demoted-errors "error marking thing: %s"
    (when-let ((conn--thing-start-pt)
               (thing (conn--command-property :conn-thing-movement-cmd)))
      (let ((thing-props (conn--command-property :conn-thing-properties)))
        (cond
         ((plist-get thing-props :handler)
          (funcall (plist-get thing-props :handler)
                   conn--thing-start-pt))
         ((plist-get thing-props :discrete)
          (pcase-let ((`(,beg . ,end) (bounds-of-thing-at-point thing)))
            (cond ((= (point) end) (conn--push-ephemeral-mark beg))
                  ((= (point) beg) (conn--push-ephemeral-mark end)))))
         ((and (/= (point) conn--thing-start-pt)
               (/= 0 (prefix-numeric-value current-prefix-arg)))
          (let* ((dir (cl-signum (- (point) conn--thing-start-pt)))
                 (dist (* dir (prefix-numeric-value current-prefix-arg))))
            (save-excursion
              (when (> (abs dist) 1)
                (forward-thing thing (- (+ dist (- dir)))))
              (funcall (or (get thing (if (> dir 0) 'beginning-op 'end-op))
                           (lambda () (forward-thing thing (- dir)))))
              (conn--push-ephemeral-mark))))))))
  (setq conn--thing-start-pt nil))
(put 'conn--thing-post-command-hook 'permanent-local-hook t)

(defun conn--init-thing-movement-cmds ()
  (pcase-dolist (`(,thing . ,cmds) conn--thing-movement-cmds)
    (pcase-dolist (`(,cmd . ,properties) cmds)
      (apply #'conn-add-thing-movement-command thing cmd properties))))

(defun conn--pop-mark-ad ()
  (when mark-ring
    (set-marker (mark-marker) (car mark-ring))
    (set-marker (car mark-ring) nil)
    (pop mark-ring))
  (deactivate-mark))

(defun conn--pop-to-mark-command-ad (&rest _)
  (unless (null (mark t))
    (add-to-history 'conn--unpop-ring (copy-marker (mark-marker)) mark-ring-max))
  (setq conn--ephemeral-mark nil))

(defun conn--buffer-bounds ()
  (cons (point-min) (point-max)))
(put 'buffer 'bounds-of-thing-at-point 'conn--buffer-bounds)

(defun conn-unpop-to-mark-command ()
  (interactive)
  (if (null conn--unpop-ring)
      (user-error "No markers to unpop")
    (when (= (point) (car conn--unpop-ring))
      (push-mark (point) t nil)
      (set-marker (pop conn--unpop-ring) nil))
    (goto-char (marker-position (car conn--unpop-ring)))
    (push-mark (point) t nil)
    (set-marker (pop conn--unpop-ring) nil)))

(defun conn--setup-mark ()
  (when conn--update-mark-cursor
    (cancel-timer conn--update-mark-cursor)
    (setq conn--update-mark-cursor nil))
  (when conn-mode
    (conn--init-thing-movement-cmds)
    (mapc #'conn-add-mark-trail-command conn-mark-trail-commands)
    (setq conn--update-mark-cursor
          (run-with-idle-timer 0.025 t #'conn--update-mark-cursors))
    (pcase-dolist (`(,mode . ,states) conn-hide-mark-cursor-modes)
      (dolist (state states)
        (conn-hide-mark-cursor mode state)))))

(defun conn--reset-mark-cursor ()
  (save-restriction
    (widen)
    (dolist (ov (conn--all-overlays 'conn--mark-cursor-p (point-min) (point-max)))
      (delete-overlay ov)))
  (setq conn--mark-cursor nil))

(provide 'conn-mark)
;;; conn-mark.el ends here
