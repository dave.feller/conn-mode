;;; conn-keymaps.el --- Keymap definitions for conn-mode -*- lexical-binding: t -*-
;;
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.
;;
;;; Commentary:
;;
;; Keybindings for conn-mode
;;
;;; Code:

(require 'conn-core)
(require 'compat)

;;;; Keymaps

(define-conn-mode-map '(conn-state emacs-state dot-state)
                      'occur-mode
                      (define-keymap "C-c e" 'occur-edit-mode))

(define-conn-mode-map '(conn-state emacs-state dot-state)
                      'occur-edit-mode
                      (define-keymap "C-c e" 'occur-cease-edit))

(define-conn-mode-map 'emacs-state 'occur-mode
                      (define-keymap "C-. b" 'conn-dot-occur-matches))

(defvar-keymap conn-tab-bar-map
  :prefix 'conn-tab-bar-map
  :doc "Keymap for tab-bar commands."
  "j" 'conn-tab-bar-new-named-tab
  "u" 'tab-close
  "o" 'tab-close-other
  "i" 'tab-rename
  "h" 'conn-tab-bar-duplicate-and-name-tab
  "." 'other-tab-prefix
  "v" 'tab-undo
  "n" 'tab-bar-switch-to-next-tab
  "p" 'tab-bar-switch-to-prev-tab
  "k" 'tab-switch
  "m" 'tab-detach)

(define-keymap
  :keymap indent-rigidly-map
  "l" 'indent-rigidly-right
  "j" 'indent-rigidly-left
  "L" 'indent-rigidly-right-to-tab-stop
  "J" 'indent-rigidly-left-to-tab-stop)

(defvar-keymap conn-tab-bar-repeat-map
  :repeat t
  "n" 'tab-bar-switch-to-next-tab
  "p" 'tab-bar-switch-to-prev-tab)

(defvar-keymap conn-narrow-map
  :doc "Keymap for hiding and highlighting functions."
  "." 'highlight-symbol-at-point
  "," 'conn-highlight-region
  "V" 'hs-hide-level
  "t" 'hs-toggle-hiding
  "X" 'highlight-regexp
  "Z" 'highlight-phrase
  "K" 'highlight-lines-matching-regexp
  "k" 'unhighlight-regexp
  "b" 'hs-hide-block
  "e" 'hs-show-block
  "i" 'hs-show-all
  "f" 'hs-hide-all)

(defvar-keymap conn-mode-map
  "C-x n" conn-narrow-map
  "C-M-<backspace>" 'backward-kill-sexp
  "C-M-<return>" 'default-indent-new-line
  "S-<return>" 'conn-open-line-and-indent
  "<pause>" 'conn-toggle-minibuffer-focus
  "M-O" 'pop-to-mark-command
  "M-U" 'conn-unpop-to-mark-command)

(defvar-keymap conn-dot-this-map
  :prefix 'conn-dot-this-map
  :doc "Dot this map."
  "P" 'conn-dot-text-property
  "u" 'conn-dot-skip-backward
  "o" 'conn-dot-skip-forward
  "l" 'conn-dot-region-forward
  "j" 'conn-dot-region-backward
  "w" 'conn-remove-dot-backward
  "e" 'conn-remove-dot-forward)

(define-keymap
  :keymap conn-dot-this-map
  "t" 'conn-add-dots-matching-region
  "a" 'conn-dot-all-things-in-region)

(define-keymap
  :keymap dot-state-transition-map
  "Q" 'conn-dot-quit
  "T" 'conn-dots-change
  "F" 'conn-dots-insert
  "C" 'conn-dots-command
  "A" 'conn-apply-on-dots
  "G" 'conn-dots-insert-after
  "H" 'conn-yank-dots-to-region
  "Y" 'conn-yank-region-to-dots
  ";" 'conn-comment-or-uncomment-dots
  "S-<f8>" 'conn-dots-command
  "<f8>" 'conn-state)

(define-keymap
  :keymap isearch-mode-map
  "<return>" 'conn-isearch-exit-and-mark
  "C-<return>" 'conn-isearch-exit-end
  "M-<return>" 'conn-isearch-exit-beginning
  "M-q" 'conn-isearch-done-and-clear-dots
  "M-." 'conn-isearch-add-dots
  "C-." 'conn-isearch-dot-map)

(defvar-keymap conn-isearch-dot-map
  :prefix 'conn-isearch-dot-map
  :repeat t
  "C-d" 'conn-isearch-add-dots
  "C-w" 'conn-isearch-remove-dots
  "C-x" 'conn-isearch-split-dots
  "C-j" 'conn-isearch-dot-jump-to-register
  "C-W" 'conn-isearch-delete-inside-dots)

(define-keymap
  :keymap conn-isearch-dot-map
  "C-f" 'conn-isearch-dot-insert
  "M-f" 'conn-isearch-dot-insert-after
  "C-e" 'conn-isearch-dot-change
  "C-v" 'conn-isearch-dot-command
  "C-=" 'conn-comment-or-uncomment-dots
  "C-n" 'conn-apply-on-dots
  "C-q" 'conn-remove-all-dots
  "C-a" 'conn-remove-dots-after
  "C-b" 'conn-remove-dots-before
  "C-\\" 'conn-dot-trim-whitespace)

(defvar-keymap conn-window-map
  :prefix 'conn-window-map
  :doc "conn window command keymap."
  "b" 'bury-buffer
  "v" 'winner-undo
  "z" 'winner-redo
  "c" 'enlarge-window-horizontally
  "x" 'shrink-window-horizontally
  "e" 'enlarge-window
  "s" 'shrink-window
  "2" 'tear-off-window
  "d" 'conn-set-window-dedicated)

(defvar-keymap conn-winner-repeat-map
  :repeat t
  "v" 'winner-undo
  "V" 'winner-undo
  "z" 'winner-redo
  "Z" 'winner-redo)

(defvar-keymap conn-compile-mode-map
  "<" 'previous-error-no-select
  ">" 'next-error-no-select)
(define-conn-mode-map 'conn-state 'compilation-mode conn-compile-mode-map)

(define-keymap
  :keymap goto-map
  "s F" 'multi-isearch-files-regexp
  "s B" 'multi-isearch-buffers-regexp
  "s f" 'multi-isearch-files
  "s b" 'multi-isearch-buffers
  "h" 'imenu
  "Y" 'pop-global-mark
  "q" 'locate
  "j" 'occur
  "k" 'goto-line
  "a" 'xref-find-apropos
  "/" 'xref-find-references
  "C-." 'conn-xref-definition-prompt
  ">" 'next-error
  "<" 'previous-error)

(defvar-keymap conn-rectangle-mode-map
  "*" 'calc-grab-rectangle
  "+" 'calc-grab-sum-down
  "_" 'calc-grab-sum-across
  "y" 'yank-rectangle)
(define-conn-mode-map 'conn-state 'rectangle-mark-mode conn-rectangle-mode-map)

(defvar-keymap conn-frame-map
  :prefix 'conn-frame-map
  "o" 'find-file-read-only-other-frame
  "n" 'make-frame-command
  "k" 'delete-frame
  "f" 'find-file-other-frame
  "d" 'dired-other-frame
  "b" 'switch-to-buffer-other-frame)

(defvar-keymap conn-buffer-map
  :prefix 'conn-buffer-map
  "w" 'previous-buffer
  "r" 'next-buffer
  "d" 'conn-buffer-dedicated-mode
  "v" 'revert-buffer-with-fine-grain
  "A" 'append-to-file
  "x" 'server-edit
  "b" 'ibuffer
  "h" 'highlight-compare-buffers
  "f" 'diff-buffer-with-file
  "E" 'ediff-buffers
  "e" 'ediff-files
  "c" 'copy-to-buffer
  "p" 'prepend-to-buffer
  "a" 'append-to-buffer
  "t" 'tear-off-window)

(defvar-keymap conn-buffer-repeat-map
  :repeat t
  "w" 'previous-buffer
  "r" 'next-buffer)

(defvar-keymap conn-misc-edit-map
  :prefix 'conn-misc-edit-map
  "<return>" 'whitespace-cleanup
  "SPC" 'conn-mark-thing
  "c -" 'conn-kebab-case-region
  "c c" 'capitalize-region
  "c d" 'downcase-region
  "c u" 'upcase-region
  "c o" 'conn-capital-case-region
  "c a" 'conn-camel-case-region
  "c S" 'conn-capital-snake-case-region
  "c s" 'conn-snake-case-region
  "s r" 'sort-regexp-fields
  "s n" 'sort-numeric-fields
  "s p" 'sort-paragraphs
  "s a" 'sort-pages
  "s c" 'sort-columns
  "s l" 'sort-lines
  "a u" 'align-unhighlight-rule
  "a h" 'align-highlight-rule
  "a n" 'align-newline-and-indent
  "a e" 'align-entire
  "a c" 'align-current
  "a r" 'align-regexp
  "f l" 'center-line
  "f u" 'fill-region-as-paragraph
  "f r" 'fill-region
  "f c" 'set-fill-column
  "f p" 'fill-paragraph
  "g" 'conn-duplicate-region
  "d" 'duplicate-line
  "h" 'conn-region-case-dwim
  "F" 'fill-paragraph
  "t" 'conn-transpose-things-at-pts
  "o" 'transpose-words
  "u" 'conn-transpose-words-backward
  "m" 'transpose-sexps
  "n" 'conn-transpose-sexps-backward
  "K" 'transpose-paragraphs
  "I" 'conn-transpose-paragraphs-backward
  "k" 'transpose-lines
  "i" 'conn-transpose-lines-backward
  "l" 'transpose-chars
  "j" 'conn-transpose-chars-backward
  "r" 'replace-regexp
  "R" 'query-replace-regexp
  "w" 'replace-string
  "q" 'query-replace)

(define-keymap
  :keymap ctl-x-r-map
  "\\" 'conn-set-register-seperator
  "." 'conn-last-dot-macro-to-register
  "," 'conn-dot-state-to-register
  "!" 'kmacro-to-register
  "W" 'conn-clear-register)

(define-keymap
  :keymap dot-state-map
  "$" 'conn-add-dots-matching-string
  "'" 'conn-remove-dots-outside
  "(" 'conn-add-dots-matching-regexp
  "<backspace>" 'conn-kill-to-dots
  "<return>" 'conn-dot-lines
  "<tab>" 'conn-remove-all-dots
  "C-<return>" 'conn-split-dots-on-newline
  "C-b" 'conn-dot-back
  "C-f" 'conn-dot-front
  "C-n" 'conn-next-dot
  "C-p" 'conn-previous-dot
  "M-<backspace>" 'conn-delete-all-dot-regions
  "M-<" 'conn-rotate-dots-backward
  "M->" 'conn-rotate-dots
  "R" 'conn-dot-redo
  "V" 'conn-dot-undo
  "[" 'conn-remove-dots-before
  "\\" 'conn-split-dots-on-regexp
  "|" 'conn-split-region-on-regexp
  "]" 'conn-remove-dots-after
  "_" 'conn-dot-trim-regexp
  "`" 'conn-dot-movement-mode
  "b" 'conn-c-x-map
  "d" 'conn-dot-region
  "h" 'conn-dot-point
  "q" 'conn-query-remove-dots
  "t" 'conn-dot-this-map
  "w" 'conn-remove-dot-at-point
  "{" 'conn-first-dot
  "}" 'conn-last-dot
  "M-<down-mouse-1>" 'conn-dot-at-click)

(define-keymap
  :keymap conn-state-map
  "'" 'quoted-insert
  "\\" 'quit-window
  "Q" 'kill-buffer-and-window
  "<f7>" 'conn-tab-bar-map
  "?" 'other-frame-prefix
  "_" 'cycle-spacing
  "}" 'conn-change-pair
  "|" 'conn-collapse-whitespace-pair
  "{" 'conn-delete-pair
  "w" 'conn-kill-region
  "y" 'conn-yank
  "q" 'conn-misc-edit-map
  "/" 'other-window-prefix
  "[" 'conn-kill-append-region
  "\"" 'conn-insert-pair
  "]" 'conn-kill-prepend-region
  "M-y" 'conn-completing-yank-replace
  "M-K" 'conn-drag-region-up
  "M-I" 'conn-drag-region-down
  "C-y" 'conn-yank-replace
  "*" 'calc-dispatch
  "%" 'shell-command-on-region
  "d" 'conn-C-d
  "g" 'conn-c-c-map
  "b" 'conn-c-x-map
  "Y" 'conn-yank-from-kill-ring-in-context
  "`" 'conn-buffer-map
  "C" 'conn-copy-region
  "V" 'winner-undo
  "Z" 'winner-redo)

(define-keymap
  :keymap conn-common-map
  "+" 'conn-set-register-seperator
  "SPC" 'conn-set-mark-command
  "u" 'backward-word
  "o" 'forward-word
  "n" 'backward-sexp
  "m" 'forward-sexp
  "l" 'forward-char
  "j" 'backward-char
  "L" 'conn-end-of-inner-line
  "J" 'conn-beginning-of-inner-line
  "k" 'next-line
  "i" 'previous-line
  "K" 'forward-paragraph
  "I" 'backward-paragraph
  "<" 'beginning-of-defun
  ">" 'end-of-defun
  "M" 'forward-line
  "N" 'conn-backward-line
  "(" 'conn-backward-whitespace
  ")" 'forward-whitespace
  "O" 'forward-sentence
  "U" 'backward-sentence
  "@" 'kmacro-end-or-call-macro
  "!" 'kmacro-start-macro-or-insert-counter
  "c" 'conn-toggle-mark-command
  "x" 'conn-exchange-mark-command
  "s" goto-map
  "p" 'conn-register-load
  "a" 'execute-extended-command
  "A" 'execute-extended-command-for-buffer
  "v" 'undo)

;;;; Repeatable Commands

(mapc 'conn-set-repeat-command
      '(transpose-words
        transpose-sexps
        conn-duplicate-region
        conn-set-window-dedicated
        previous-error
        next-error
        pop-global-mark
        conn-region-case-dwim
        transpose-chars
        transpose-lines
        duplicate-line
        transpose-paragraphs))

;;;; Keymap Setup

(defun conn--setup-keymaps ()
  (if conn-mode
      (progn
        (cl-pushnew 'conn--state-maps emulation-mode-map-alists)
        (cl-pushnew 'conn--aux-maps emulation-mode-map-alists)
        (cl-pushnew 'conn--local-maps emulation-mode-map-alists)
        (cl-pushnew 'conn--overriding-maps emulation-mode-map-alists)
        (cl-pushnew 'conn--local-mode-maps emulation-mode-map-alists)
        (cl-pushnew 'conn--transition-maps emulation-mode-map-alists)
        (unless conn--keymap-idle-timer
          (setq conn--keymap-idle-timer
                (run-with-idle-timer 0.75 t 'conn--setup-aux-maps-in-selected-win)))
        (define-keymap
          :keymap undo-repeat-map
          "u" `(menu-item "Undo" undo
                          :filter ,(lambda (_) (when emacs-state 'undo)))))
    (when conn--keymap-idle-timer
      (cancel-timer conn--keymap-idle-timer)
      (setq conn--keymap-idle-timer nil))
    (setq emulation-mode-map-alists
          (seq-difference '(conn--state-maps
                            conn--aux-maps
                            conn--local-maps
                            conn--overriding-maps
                            conn--local-mode-maps
                            conn--transition-maps)
                          emulation-mode-map-alists #'eq))
    (define-keymap :keymap undo-repeat-map "u" 'undo)))

(provide 'conn-keymaps)
;;; conn-keymaps.el ends here
