;;; conn-core.el --- this file is not part of GNU Emacs. -*- lexical-binding: t -*-
;;
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.
;;
;;; Commentary:
;;
;; Core functionality for conn-mode
;;
;;; Code:

;;;; Requires
(require 'compat)
(require 'rect)
(require 'elec-pair)
(require 'isearch)
(require 'misearch)
(require 'winner)
(require 'conn-utils)
(require 'repeat)
(eval-when-compile
  (require 'cl-lib)
  (require 'subr-x))

;;;; Variables
;;;;; Custom Variables

(defgroup conn-mode nil
  "Modal keybinding mode."
  :prefix "conn-"
  :group 'conn-mode)

(defcustom conn-lighter " Conn"
  "Modeline lighter for conn-mode."
  :group 'conn-mode
  :type 'string)

(defcustom conn-state-buffer-colors nil
  "Indicate state using buffer faces."
  :group 'conn-mode
  :type 'boolean)

(defcustom conn-mode-line-indicator t
  "Conn state mode line indicator"
  :group 'conn-mode
  :type 'boolean)

(defface conn-state-buffer-face
  '((t :inherit default :background "#f7eee1"))
  "Face for Emacs state mode line indicator."
  :group 'conn-mode)

(defface dot-state-buffer-face
  '((t :inherit default :background "#eff7ef"))
  "Face for Emacs state mode line indicator."
  :group 'conn-mode)

(defface emacs-state-buffer-face
  '((t :inherit default :background "#f8f8f8"))
  "Face for Emacs state mode line indicator."
  :group 'conn-mode)

(defface conn-state-lighter-face
  '((t (:background "#f3bdbd" :box (:line-width 2 :color "#7a1a1a"))))
  "Face for command state mode line indicator."
  :group 'conn-mode)

(defface conn-dot-state-ligher-face
  '((t (:background "#d1ead5" :box (:line-width 2 :color "#33553d"))))
  "Face for dots state mode line indicator."
  :group 'conn-mode)

(defface conn-emacs-state-ligher-face
  '((t (:background "#cae1ff" :box (:line-width 2 :color "#355687"))))
  "Face for Emacs state mode line indicator."
  :group 'conn-mode)

(defcustom conn-default-state 'conn-state
  "Default conn state for new buffers."
  :type 'symbol
  :group 'conn-mode)

(defcustom conn-buffer-default-state-alist nil
  "Alist of the form ((REGEXP . STATE) ...).
Defines default STATE for buffers matching REGEXP."
  :type '(list (cons string symbol))
  :group 'conn-mode)

(defcustom conn-state-cursor-type 'box
  "`cursor-type' for conn-state."
  :type '(choice
          (const :tag "Frame default" t)
          (const :tag "Filled box" box)
          (cons :tag "Box with specified size"
                (const box)
                integer)
          (const :tag "Hollow cursor" hollow)
          (const :tag "Vertical bar" bar)
          (cons :tag "Vertical bar with specified height"
                (const bar)
                integer)
          (const :tag "Horizontal bar" hbar)
          (cons :tag "Horizontal bar with specified width"
                (const hbar)
                integer)
          (const :tag "None " nil))
  :group 'conn-mode)

(defcustom dot-state-cursor-type 'box
  "`cursor-type' for dot-state."
  :type '(choice
          (const :tag "Frame default" t)
          (const :tag "Filled box" box)
          (cons :tag "Box with specified size"
                (const box)
                integer)
          (const :tag "Hollow cursor" hollow)
          (const :tag "Vertical bar" bar)
          (cons :tag "Vertical bar with specified height"
                (const bar)
                integer)
          (const :tag "Horizontal bar" hbar)
          (cons :tag "Horizontal bar with specified width"
                (const hbar)
                integer)
          (const :tag "None " nil))
  :group 'conn-mode)

(defcustom emacs-state-cursor-type 'box
  "`cursor-type' for emacs-state."
  :type '(choice
          (const :tag "Frame default" t)
          (const :tag "Filled box" box)
          (cons :tag "Box with specified size"
                (const box)
                integer)
          (const :tag "Hollow cursor" hollow)
          (const :tag "Vertical bar" bar)
          (cons :tag "Vertical bar with specified height"
                (const bar)
                integer)
          (const :tag "Horizontal bar" hbar)
          (cons :tag "Horizontal bar with specified width"
                (const hbar)
                integer)
          (const :tag "None " nil))
  :group 'conn-mode)

;;;;; Internal Vars

(defvar-local conn--mode-line-format nil
  "Mode line state indicator string.")

(defvar-local conn--input-method nil
  "Current input for buffer.")

(defvar-local conn-current-state nil
  "Current conn state for buffer.")

(defvar-local conn-previous-state nil
  "Previous conn state for buffer.")

(defvar conn--state-maps nil)
(defvar-local conn--aux-maps nil)
(defvar-local conn--local-maps (make-sparse-keymap))
(defvar conn--overriding-maps nil)
(defvar conn--mode-maps nil)
(defvar-local conn--local-mode-maps nil)
(defvar conn--transition-maps nil)

(defvar conn--keymap-idle-timer nil
  "Timer to update aux keymaps.")

;;;;; Bindings Vars

(defvar conn-kill-region-keys "C-w")
(defvar conn-backward-delete-keys "DEL")
(defvar conn-yank-keys "C-y")

(defun conn-call-binding (binding)
  (call-interactively
   (let ((emulation-mode-map-alists (seq-difference
                                     emulation-mode-map-alists
                                     '(conn--transition-maps
                                       conn--local-mode-maps
                                       conn--overriding-maps
                                       conn--local-maps
                                       conn--aux-maps
                                       conn--state-maps))))
     (keymap-lookup nil binding))))

(defun conn-macro-binding (name keys)
  (fset name (lambda ()
               (interactive)
               (let* ((emulation-mode-map-alists (seq-difference
                                                  emulation-mode-map-alists
                                                  '(conn--transition-maps
                                                    conn--local-mode-maps
                                                    conn--overriding-maps
                                                    conn--local-maps
                                                    conn--aux-maps
                                                    conn--state-maps)))
                      (last-kbd-macro keys))
                 (call-last-kbd-macro))))
  name)

;;;;; Command Histories

(defvar conn-thing-history nil)

(defvar conn--seperator-history nil
  "History var for `conn-set-register-seperator'.")

;;;; Core state functionality

(defun conn--region-beginning ()
  (let ((mark-even-if-inactive t))
    (region-beginning)))

(defun conn--region-end ()
  (let ((mark-even-if-inactive t))
    (region-end)))

(defun conn--region-bounds ()
  (let ((mark-even-if-inactive t))
    (region-bounds)))

(defun conn--derived-mode-property (property &optional buffer)
  "Check major mode in BUFFER and each `derived-mode-parent' for PROPERTY.
If BUFFER is nil check `current-buffer'."
  (with-current-buffer (or buffer (current-buffer))
    (let ((mode major-mode)
          result)
      (while (and mode (not result))
        (setq result (get mode property)
              mode (get mode 'derived-mode-parent)))
      result)))

(defun conn--setup-c-c-mode-map ()
  "Setup c-c-mode-map."
  (when-let ((mode-map (make-sparse-keymap))
             (c-c-map (keymap-canonicalize (keymap-lookup nil "C-c"))))
    (map-keymap
     (lambda (ev def)
       (let ((mods (event-modifiers ev))
             (base (event-basic-type ev)))
         (cond
          ((and (numberp ev) (= ev 27))
           (when (keymapp def)
             (map-keymap
              (lambda (ev def)
                (define-key mode-map (vector 27 ev) def))
              (keymap-canonicalize def))))
          ((not (and (characterp base)
                     (memq (get-char-code-property base 'general-category)
                           '(Ll Lu Lo Lt Lm Mn Mc Me Nl))))
           (define-key mode-map (vector ev) def))
          ((or (equal mods '(control))
               (equal mods '(control shift)))
           (let ((keys (thread-first
                         (append (remq 'control mods) (list base))
                         (event-convert-list)
                         (vector))))
             (define-key mode-map keys def))))))
     c-c-map)
    mode-map))

(defun conn--setup-aux-maps (&optional buffer)
  "Setup conn aux maps for state in BUFFER."
  (with-current-buffer (or buffer (current-buffer))
    (let ((aux-map (setf (alist-get conn-current-state conn--aux-maps)
                         (make-sparse-keymap))))
      (when-let ((c-d-keys (where-is-internal 'conn-C-d))
                 (c-d-bind (keymap-lookup nil "C-d" t)))
        (dolist (key c-d-keys)
          (define-key aux-map key c-d-bind)))
      (when-let ((c-x-keys (where-is-internal 'conn-c-x-map))
                 (c-x-map (keymap-lookup nil "C-x" t)))
        (dolist (key c-x-keys)
          (define-key aux-map key c-x-map)))
      (when-let ((c-c-keys (where-is-internal 'conn-c-c-map))
                 (c-c-map (keymap-lookup nil "C-c" t)))
        (dolist (key c-c-keys)
          (define-key aux-map key c-c-map)))
      (when-let ((c-c-mode-keys (where-is-internal 'conn-c-c-mode-map))
                 (c-c-mode-map (conn--setup-c-c-mode-map)))
        (dolist (key c-c-mode-keys)
          (define-key aux-map key c-c-mode-map))))))

(defun conn--setup-aux-maps-in-selected-win ()
  "Setup aux maps only in currently selected window."
  (conn--setup-aux-maps (window-buffer (selected-window))))

(defun conn-previous-state ()
  "Transition to previous conn state."
  (interactive)
  (funcall conn-previous-state))

(defun set-conn-transition (state key transition-fn)
  "Add TRANSITION-FN bound to KEY in STATE's transition keymap."
  (let ((keymap (alist-get state conn--transition-maps)))
    (if (null keymap)
        (error "Transition keymap not found for state %s" state)
      (define-key keymap
                  (cl-etypecase key
                    (vector key)
                    (string (kbd key)))
                  transition-fn))))

(defun unset-conn-transition (state key)
  "Remove KEY from STATE's transition keymap."
  (let ((keymap (alist-get state conn--transition-maps)))
    (if (null keymap)
        (error "Transition keymap not found for state %s" state)
      (define-key keymap
                  (cl-etypecase key
                    (vector key)
                    (string (kbd key)))
                  nil t))))

(defun define-conn-mode-map (states modes map)
  "Add a MAP for MODE in which will be active in STATES.
STATES may be either a state or a list of states."
  (dolist (state (ensure-list states))
    (dolist (mode (ensure-list modes))
      (cl-pushnew (cons mode map) (alist-get state conn--mode-maps)
                  :test #'equal))))

(defun define-conn-aux-map (states map)
  "Add MAP to each of STATES as an aux map.
STATES may be either a state or a list of states.
VARIABLE has the same meaning as in `minor-mode-map-alist'."
  (dolist (state (ensure-list states))
    (push (cons state map) conn--overriding-maps)))

(defun conn--activate-input-method ()
  "Enable input method in states with nil :conn-suppress-input-method property."
  (let (input-method-activate-hook
        input-method-deactivate-hook)
    (when (and conn-local-mode conn-current-state)
      (cond ((not (get conn-current-state :conn-suppress-input-method))
             (activate-input-method conn--input-method))
            (current-input-method
             (setq conn--input-method current-input-method)
             (deactivate-input-method))))))
(put 'conn--activate-input-method 'permanent-local-hook t)

(defun conn--deactivate-input-method ()
  "Disable input method in all states."
  (let (input-method-activate-hook
        input-method-deactivate-hook)
    (when (and conn-local-mode conn-current-state)
      (setq conn--input-method nil))))
(put 'conn--deactivate-input-method 'permanent-local-hook t)

(defun conn--default-state-for-buffer (buffer)
  "Get default state for BUFFER."
  (with-current-buffer (or buffer (current-buffer))
    (or (cdr (assoc (buffer-name buffer)
                    conn-buffer-default-state-alist
                    #'buffer-match-p))
        (conn--derived-mode-property :conn-default-state)
        conn-default-state)))

(defun set-default-conn-state (modes-or-buffers state)
  "Set default STATE for each MODES-OR-BUFFERS.

Modes are symbols tested against `major-mode'.
Buffers are strings matched using `buffer-match-p'."
  (dolist (var (ensure-list modes-or-buffers))
    (cl-etypecase var
      (symbol (put var :conn-default-state state))
      (string (push (cons var state) conn-buffer-default-state-alist)))))

(defmacro define-conn-state (name &rest body)
  "Define a conn state NAME.
Defines a transition function NAME and a variable
NAME which is t when in NAME state.

:LIGHTER-FACE is the mode-line lighter that will be displayed in NAME.

:SUPPRESS-INPUT-METHOD if non-nill suppresses current input method in NAME.

:KEYMAP is a keymap for the state.

:CURSOR is the `cursor-type' for NAME.

:TRANSITIONS is a list of transition functions to be bound in NAME's
transition map of the form ((KEY TRANSITION-FUNCTION) ...).

BODY contains code to be executed each time the transition function is executed.

\(fn STATE [KEYWORD VAL ... &rest BODY])"
  (declare (indent defun))
  (let* ((state-hook-sym (conn--symbolicate name "-hook"))
         (map-name (conn--symbolicate name "-map"))
         (transition-fn-name  (conn--symbolicate name "-transition"))
         (transition-map-name (conn--symbolicate name "-transition-map"))
         (overriding-map-name (conn--symbolicate name "-overriding-map"))
         keyw lighter-face suppress-input-method keymap cursor transitions)
    (while (keywordp (setq keyw (car body)))
      (setq body (cdr body))
      (pcase keyw
        (:cursor (setq cursor (pop body)))
        (:lighter-face (setq lighter-face (pop body)))
        (:suppress-input-method (setq suppress-input-method (pop body)))
        (:keymap (setq keymap (pop body)))
        (:transitions (setq transitions (pop body)))
        (_ (pop body))))
    `(progn
       (defvar-local ,name nil)
       (defvar ,map-name ,(cond ((null keymap)
                                 `(make-sparse-keymap))
                                ((symbolp keymap)
                                 `(symbol-value ,keymap))
                                (t keymap)))
       (defvar ,transition-map-name (make-sparse-keymap))
       (defvar ,overriding-map-name (make-sparse-keymap))

       (put ',name :conn-state-hook ',state-hook-sym)
       (put ',name :conn-cursor-type ,cursor)
       (put ',name :conn-suppress-input-method ,suppress-input-method)

       (push (cons ',name ,map-name) conn--state-maps)
       (push (cons ',name ,overriding-map-name) conn--overriding-maps)
       (push (cons ',name ,transition-map-name) conn--transition-maps)

       ,@(let (res)
           (pcase-dolist (`(,keys . ,state) transitions)
             (push `(set-conn-transition ',name ,keys ',state) res))
           res)

       (defun ,name ()
         ,(message "Activate %s in the current buffer"
                   (upcase (symbol-name name)))
         (interactive)
         (when conn-current-state (funcall (get conn-current-state :conn-transition-fn) t))
         (,transition-fn-name))

       (defun ,transition-fn-name (&optional exit)
         (unless (xor exit (eq conn-current-state ',name))
           (if exit
               (progn
                 (setq ,name nil)
                 (setq conn-current-state nil)
                 (setq conn-previous-state ',name))
             (setq conn-current-state ',name)
             (setq ,name t)
             (setq-local conn-lighter (propertize conn-lighter 'face ',lighter-face))
             (conn--activate-input-method)
             (setq conn--local-mode-maps (alist-get conn-current-state conn--mode-maps))
             (conn--setup-aux-maps)
             (conn--refresh-cursor)
             (force-mode-line-update))
           ,@body
           (run-hooks ',state-hook-sym))
         (put ',name :conn-transition-fn ',transition-fn-name)))))

;;;; State definitions

(defvar conn-common-map (make-sparse-keymap))

(defconst conn--suppress-map
  (let ((map (make-keymap)))
    (suppress-keymap map nil)
    map))

(define-conn-state emacs-state
  :lighter-face conn-emacs-state-ligher-face
  :cursor emacs-state-cursor-type
  :transitions (("<f7>" . conn-pop-state)
                ("<f8>" . conn-state)
                ("<f9>" . dot-state))
  (if emacs-state
      (progn
        (when conn-state-buffer-colors
          (buffer-face-set 'emacs-state-buffer-face))
        (conn--push-ephemeral-mark)
        (remove-hook 'pre-command-hook #'conn--mark-trail-pre-command-hook t)
        (remove-hook 'post-command-hook #'conn--thing-post-command-hook t))
    (buffer-face-mode -1)
    (add-hook 'pre-command-hook #'conn--mark-trail-pre-command-hook nil t)
    (add-hook 'post-command-hook #'conn--thing-post-command-hook nil t)))

(define-conn-state conn-state
  :lighter-face conn-state-lighter-face
  :suppress-input-method t
  :cursor conn-state-cursor-type
  :keymap
  (let ((map (make-sparse-keymap)))
    (thread-last
      (list conn-common-map conn--suppress-map)
      (make-composed-keymap)
      (set-keymap-parent map))
    map)
  :transitions (("f" . conn-emacs-state)
                ("t" . conn-change)
                ("<f7>" . emacs-state)
                ("<f8>" . conn-pop-state)
                ("<f9>" . dot-state))
  (if conn-state
      (when conn-state-buffer-colors
        (buffer-face-set 'conn-state-buffer-face))
    (buffer-face-mode -1)))

(define-conn-state dot-state
  :lighter-face conn-dot-state-ligher-face
  :suppress-input-method t
  :cursor dot-state-cursor-type
  :keymap
  (let ((map (make-sparse-keymap)))
    (thread-last
      (list conn-common-map conn--suppress-map)
      (make-composed-keymap)
      (set-keymap-parent map))
    map)
  :transitions (("<f7>" . emacs-state)
                ("<f8>" . conn-state)
                ("<f9>" . conn-pop-state))
  (if dot-state
      (progn
        (when conn-state-buffer-colors
          (buffer-face-set 'dot-state-buffer-face))
        (setq conn--dot-undo-ring nil)
        (conn--for-each-dot
         (lambda (dot)
           (push `(create ,(overlay-start dot) . ,(overlay-end dot))
                 conn--dot-this-undo)))
        (add-hook 'post-command-hook #'conn--dot-post-command-hook t t))
    (buffer-face-mode -1)
    (conn-dot-movement-mode -1)
    (setq conn--dot-undo-ring nil)
    (remove-hook 'post-command-hook #'conn--dot-post-command-hook t)))

;;;; Advice

(defun conn--repeat-get-map-ad ()
  (when-let (repeat-mode
             (prop (repeat--command-property :conn-repeat-command))
             (m (or (eq prop t)
                    (eq prop conn-current-state))))
    (define-keymap (single-key-description last-command-event) this-command)))

(defun conn-set-repeat-command (command)
  (put command :conn-repeat-command t))

(defun conn--calc-dispatch-ad (fn &rest args)
  (let ((buffer (current-buffer))
        (state conn-current-state))
    (set state nil)
    (unwind-protect
        (apply fn args)
      (with-current-buffer buffer
        (set state t)))))

(defun conn--copy-region-ad (beg end &optional region)
  (if region
      (pulse-momentary-highlight-region (conn--region-beginning) (conn--region-end) 'region)
    (pulse-momentary-highlight-region beg end 'region)))

(defun conn--quiet-in-isearch-ad ()
  (not isearch-mode))

(defun conn--insert-register-ad (_register &optional _arg)
  "`delete-region' or `delete-rectangle' before `insert-register'
when `use-region-p' is non-nil."
  (when (use-region-p)
    (if (rectangle-mark-mode)
        (delete-rectangle (conn--region-beginning) (conn--region-end))
      (delete-region (conn--region-beginning) (conn--region-end)))))

(defun conn--repeat-isearch-msg-ad (fn map)
  (if isearch-mode
      (let ((message-log-max nil))
        (if map
            (let ((message "Repeating"))
              (if (current-message)
                  (message "%s [%s]" (current-message) message)
                (message "%s" message)))
          (let ((message (current-message)))
            (when (and message
                       (string-search " [Repeating] " message))
              (message "%s" (replace-regexp-in-string
                             " \\[Repeat\\] " "" message))))))
    (funcall fn map)))

(defun conn--setup-advice ()
  (if conn-mode
      (progn
        (advice-add 'repeat-get-map :after-until 'conn--repeat-get-map-ad)
        (advice-add 'calc-dispatch :around 'conn--calc-dispatch-ad)
        (advice-add 'insert-register :before #'conn--insert-register-ad)
        (advice-add 'copy-region-as-kill :before #'conn--copy-region-ad)
        (advice-add 'eldoc-display-message-no-interference-p
                    :after-while #'conn--quiet-in-isearch-ad)
        (advice-add repeat-echo-function :around #'conn--repeat-isearch-msg-ad)
        (advice-add 'push-mark :around #'conn--push-mark-ad)
        (advice-add 'pop-to-mark-command :before #'conn--pop-to-mark-command-ad)
        (advice-add 'pop-mark :override #'conn--pop-mark-ad))
    (advice-remove 'repeat-get-map 'conn--repeat-get-map-ad)
    (advice-remove 'calc-dispatch 'conn-calc-dispatch-ad)
    (advice-remove 'insert-register #'conn--insert-register-ad)
    (advice-remove 'copy-region-as-kill #'conn--copy-region-ad)
    (advice-remove 'eldoc-display-message-no-interference-p
                   #'conn--quiet-in-isearch-ad)
    (advice-remove repeat-echo-function #'conn--repeat-isearch-msg-ad)
    (advice-remove 'push-mark #'conn--push-mark-ad)
    (advice-remove 'pop-to-mark-command #'conn--pop-to-mark-command-ad)
    (advice-remove 'pop-mark #'conn--pop-mark-ad)))

;;;; Commands
;;;;; Transition Functions

(defun conn-emacs-state (&optional arg)
  "Transition to emacs-state.

If ARG is non-negative open a new line below point and enter insert state.

If ARG is negative open a new line above point and enter insert state."
  (interactive "P")
  (cond ((null arg))
        ((>= (prefix-numeric-value arg) 0)
         (move-end-of-line 1)
         (newline-and-indent))
        ((< (prefix-numeric-value arg) 0)
         (move-beginning-of-line 1)
         (conn-open-line-and-indent 1)
         (indent-according-to-mode)))
  (emacs-state))

(defun conn-change (start end &optional kill)
  "Change region between START and END.

If KILL is non-nill add region to the `kill-ring'."
  (interactive (list (conn--region-beginning)
                     (conn--region-end)
                     current-prefix-arg))
  (cond ((and rectangle-mark-mode kill)
         (copy-rectangle-as-kill start end)
         (call-interactively #'string-rectangle))
        (rectangle-mark-mode
         (call-interactively #'string-rectangle))
        (kill
         (conn-kill-region)
         (emacs-state))
        (t
         (let (kill-ring)
           (conn-kill-region))
         (emacs-state))))

;;;;; Editing Commands

(defun conn-C-d ()
  "Sentinal for C-d bind.

`conn--setup-aux-maps' will rebind this to the binding of C-d in
the currently active keymaps."
  (interactive)
  (user-error "C-d bind not found"))

(defun conn-c-x-map ()
  "Sentinal for C-x map.

`conn--setup-aux-maps' will rebind this to the C-x keymap."
  (interactive)
  (user-error "C-x map not found"))

(defun conn-c-c-map ()
  "Sentinal for C-c map.

`conn--setup-aux-maps' will rebind this to the C-c keymap."
  (interactive)
  (user-error "C-c map not found"))

(defun conn-c-c-mode-map ()
  "Sentinal for C-c mode map.

`conn--setup-aux-maps' will scan the C-c prefix map, remove all user
reserved bindings (lower and upper case letters) and rebind all C-<letter>
binds to <letter>."
  (interactive)
  (user-error "C-c mode map not found"))

(defun conn-pop-state ()
  "Return to previous state."
  (interactive)
  (funcall conn-previous-state))

(defun conn-toggle-minibuffer-focus ()
  "Toggle input focus between minibuffer and `other-window'."
  (interactive)
  (cond ((not (active-minibuffer-window))
         (user-error "Minibuffer is not active"))
        ((eq (selected-window) (active-minibuffer-window))
         (other-window 1)
         (setq this-command 'other-window)
         (message "Switched to %s" (current-buffer)))
        (t
         (select-window (active-minibuffer-window))
         (message "Switched to *MINIBUFFER*"))))

(defun conn--apply-region-transform (transform-func)
  "Apply TRANSFORM-FUNC to region contents.
Handles rectangular regions."
  (save-excursion
    (let* ((case-fold-search nil))
      (if (null rectangle-mark-mode)
          (with-restriction
            (conn--region-beginning) (conn--region-end)
            (funcall transform-func))
        (apply-on-rectangle
         (lambda (start-col end-col)
           (with-restriction
             (+ (point) start-col) (+ (point) end-col)
             (funcall transform-func)))
         (conn--region-beginning) (conn--region-end))))))

(defun conn-region-case-dwim ()
  "Cycle case in region.

not (or downcase Capitalize UPCASE) ->
downcase -> Capitalize -> UPCASE -> downcase."
  (interactive)
  (conn--apply-region-transform
   (lambda ()
     (let ((str (buffer-string)))
       (cond ((string= str (capitalize str))
              (upcase-region (point-min) (point-max)))
             ((string= str (downcase str))
              (capitalize-region (point-min) (point-max)))
             (t (downcase-region (point-min) (point-max)))))
     (current-buffer))))

(defun conn-kebab-case-region ()
  "Transform region text to capital-snake-case."
  (interactive)
  (conn--apply-region-transform
   (lambda ()
     (while (re-search-forward "\\([a-z0-9]\\)\\([A-Z]\\)" nil t)
       (replace-match "\\1-\\2" nil nil))
     (goto-char (point-min))
     (while (re-search-forward "\\([A-Z]+\\)\\([A-Z][a-z]\\)" nil t)
       (replace-match "\\1-\\2" nil nil))
     (goto-char (point-min))
     (while (re-search-forward "_" nil t)
       (replace-match "-" nil nil))
     (goto-char (point-min))
     (while (re-search-forward "-+" nil t)
       (replace-match "-" nil nil))
     (downcase-region (point-min) (point-max))
     (current-buffer))))

(defun conn-capital-snake-case-region ()
  "Transform region text to Capital_Snake_Case."
  (interactive)
  (conn--apply-region-transform
   (lambda ()
     (while (re-search-forward "\\([a-z0-9]\\)\\([A-Z]\\)" nil t)
       (replace-match "\\1_\\2" nil nil))
     (goto-char (point-min))
     (while (re-search-forward "\\([A-Z]+\\)\\([A-Z][a-z]\\)" nil t)
       (replace-match "\\1_\\2" nil nil))
     (goto-char (point-min))
     (while (search-forward "-" nil t)
       (replace-match "_" nil nil))
     (goto-char (point-min))
     (while (re-search-forward "_+" nil t)
       (replace-match "_" nil nil))
     (capitalize-region (point-min) (point-max))
     (current-buffer))))

(defun conn-snake-case-region ()
  "Transform region text to snake_case."
  (interactive)
  (conn--apply-region-transform
   (lambda ()
     (while (re-search-forward "\\([a-z0-9]\\)\\([A-Z]\\)" nil t)
       (replace-match "\\1_\\2" nil nil))
     (goto-char (point-min))
     (while (re-search-forward "\\([A-Z]+\\)\\([A-Z][a-z]\\)" nil t)
       (replace-match "\\1_\\2" nil nil))
     (goto-char (point-min))
     (while (re-search-forward "-" nil t)
       (replace-match "_" nil nil))
     (goto-char (point-min))
     (while (re-search-forward "_+" nil t)
       (replace-match "_" nil nil))
     (downcase-region (point-min) (point-max))
     (current-buffer))))

(defun conn-capital-case-region ()
  "Transform region text to CapitalCase."
  (interactive)
  (conn--apply-region-transform
   (lambda ()
     (while (re-search-forward "\\([a-z0-9]\\)\\([A-Z]\\)" nil t)
       (replace-match "\\1_\\2" nil nil))
     (goto-char (point-min))
     (while (re-search-forward "\\([A-Z]+\\)\\([A-Z][a-z]\\)" nil t)
       (replace-match "\\1_\\2" nil nil))
     (capitalize-region (point-min) (point-max))
     (goto-char (point-min))
     (while (re-search-forward "[-_]+" nil t)
       (replace-match "" nil nil))
     (current-buffer))))

(defun conn-camel-case-region ()
  "Transform region text to camelCase."
  (interactive)
  (conn--apply-region-transform
   (lambda ()
     (while (re-search-forward "\\([a-z0-9]\\)\\([A-Z]\\)" nil t)
       (replace-match "\\1_\\2" nil nil))
     (goto-char (point-min))
     (while (re-search-forward "\\([A-Z]+\\)\\([A-Z][a-z]\\)" nil t)
       (replace-match "\\1_\\2" nil nil))
     (capitalize-region (point-min) (point-max))
     (goto-char (point-min))
     (while (re-search-forward "\\([A-Z]+\\)\\([A-Z][a-z]\\)" nil t)
       (replace-match "" nil nil))
     (downcase-region (point-min) (1+ (point-min)))
     (current-buffer))))

(defun conn-kill-append-region (beg end &optional register)
  "Kill region from BEG to END and append it to most recent kill.

Optionally if REGISTER is specified append kill to REGISTER instead.
When called interactively with a non-nil prefix argument read register
interactively."
  (interactive
   (list (conn--region-beginning)
         (conn--region-end)
         (when current-prefix-arg
           (register-read-with-preview "Append kill to register: "))))
  (if register
      (append-to-register register beg end t)
    (kill-append (filter-buffer-substring beg end t) t)))

(defun conn-kill-prepend-region (beg end &optional register)
  "Kill region from BEG to END and prepend it to most recent kill.

Optionally if REGISTER is specified prepend kill to REGISTER instead.
When called interactively with a non-nil prefix argument read register
interactively."
  (interactive (list (conn--region-beginning)
                     (conn--region-end)
                     (when current-prefix-arg
                       (register-read-with-preview "Prepend kill to register: "))))
  (if register
      (prepend-to-register register beg end t)
    (kill-append (filter-buffer-substring beg end t) nil)))

(defun conn-mark-thing (thing)
  "Mark THING at point."
  (interactive (list (intern
                      (completing-read
                       (format "Thing: ")
                       (conn--things 'conn--defined-thing-p) nil nil nil
                       'conn-thing-history))))
  (when-let ((bounds (bounds-of-thing-at-point thing)))
    (goto-char (car bounds))
    (conn--push-ephemeral-mark (cdr bounds))))

(defun conn-narrow-to-thing (thing)
  "Narrow to THING at point."
  (interactive (list (intern
                      (completing-read
                       (format "Thing: ")
                       (conn--things 'conn--defined-thing-p) nil nil nil
                       'conn-thing-history))))
  (when-let ((bounds (bounds-of-thing-at-point thing)))
    (narrow-to-region (car bounds) (cdr bounds))))

(defun conn-drag-region-up (beg end N)
  "Drag region from BEG to END up N lines."
  (interactive (list (conn--region-beginning)
                     (conn--region-end)
                     (or current-prefix-arg 1)))
  (let ((region (delete-and-extract-region beg end)))
    (forward-line N)
    (set-mark (point))
    (insert region)))

(defun conn-drag-region-down (beg end N)
  "Drag region from BEG to END down N lines."
  (interactive (list (conn--region-beginning)
                     (conn--region-end)
                     (or current-prefix-arg 1)))
  (conn-drag-region-up beg end (- N)))

(defun conn-collapse-whitespace (&optional all)
  "Collapse all whitespace around point into a single space or newline.

If ALL is non-nil delete all whitespace around point."
  (interactive "P")
  (save-excursion
    (save-match-data
      (re-search-backward "[^ \t\r\n]" nil t)
      (re-search-forward "[ \t\r\n]+" nil t)
      (cond (all
             (replace-match "" nil nil))
            ((string-match-p "\n" (match-string 0))
             (replace-match "\n" nil nil))
            (t
             (replace-match " " nil nil))))))

(defun conn-collapse-whitespace-pair (&optional all)
  "`conn-collapse-whitespace' at both `point' and `mark'.

If ALL is non-nil delete all whitespace around point."
  (interactive "P")
  (conn-collapse-whitespace all)
  (save-excursion
    (goto-char (mark-marker))
    (conn-collapse-whitespace all)))

(defun conn-insert-pair (beg end string)
  "Insert STRING at BEG and END.

When called interactively inserts STRING at `point' and `mark'."
  (interactive (list (conn--region-beginning)
                     (conn--region-end)
                     (minibuffer-with-setup-hook
                         (lambda ()
                           (electric-pair-local-mode 1))
                       (read-string "Pair: " nil 'conn-pair-history))))
  (let ((front (seq-subseq string 0 (floor (length string) 2)))
        (back (seq-subseq string (floor (length string) 2))))
    (save-mark-and-excursion
      (goto-char end)
      (insert-before-markers back)
      (goto-char beg)
      (insert front))))

(defun conn-change-pair (string arg)
  "Call `delete-pair' with ARG and then call `insert-pair' with STRING."
  (interactive (list (read-string "Pair: " nil 'conn-pair-history)
                     current-prefix-arg))
  (conn-delete-pair (or arg 1))
  (conn-insert-pair (conn--region-beginning)
                    (conn--region-end)
                    string))

(defun conn-delete-pair (arg)
  "Delete ARG chars at `point' and `mark'."
  (interactive "p")
  (save-mark-and-excursion
    (let ((end (> (point) (mark-marker))))
      (when end (exchange-point-and-mark t))
      (delete-region (point) (+ (point) arg))
      (delete-region (- (mark-marker) arg) (mark-marker)))))

(defun conn-backward-line (N)
  "`forward-line' by N but backward."
  (interactive "p")
  (forward-line (- N)))

(defun conn-backward-whitespace (N)
  "`forward-whitespace' by N but backward."
  (interactive "p")
  (forward-whitespace (- N)))

(defun conn-set-register-seperator (string)
  "Set `register-seperator' register to string STRING."
  (interactive
   (list (minibuffer-with-setup-hook
             (lambda ()
               (push-mark (minibuffer--completion-prompt-end) t t))
           (read-string "Seperator: " (let ((reg (get-register register-separator)))
                                        (when (stringp reg) reg))
                        conn--seperator-history nil t))))
  (if register-separator
      (set-register register-separator string)
    (user-error "Register-seperator not set")))

(defun conn-toggle-mark-command (&optional arg)
  "Toggle `mark-active'.

With a prefix ARG activate `rectangle-mark-mode'."
  (interactive "P")
  (cond (mark-active (deactivate-mark))
        (arg (activate-mark)
             (rectangle-mark-mode))
        (t (activate-mark))))

(defun conn-set-mark-command (&optional arg)
  (interactive "P")
  (cond (arg
         (rectangle-mark-mode 'toggle))
        ((eq last-command 'conn-set-mark-command)
         (if (region-active-p)
             (progn
               (deactivate-mark)
               (message "Mark deactivated"))
           (activate-mark)
           (message "Mark activated")))
        (t
         (push-mark-command nil))))

(defun conn-exchange-mark-command (&optional arg)
  "`exchange-mark-and-point' avoiding activating the mark.

With a prefix ARG `push-mark' without activating it."
  (interactive "P")
  (cond (arg
         (push-mark (point) t nil)
         (message "Marker pushed"))
        (t
         (exchange-point-and-mark (not mark-active)))))

(defun conn-join-lines (start end)
  "`delete-indentation' in region from START and END."
  (interactive (list (conn--region-beginning)
                     (conn--region-end)))
  (delete-indentation nil start end)
  (indent-according-to-mode))

(defun conn-highlight-region (start end)
  "`highlight-phrase' in region from START and END."
  (interactive (list (conn--region-beginning)
                     (conn--region-end)))
  (highlight-phrase (buffer-substring start end)))

(defun conn-replace-char (char)
  "Replace a single character with CHAR."
  (interactive "cChar:")
  (save-excursion
    (if (use-region-p)
        (delete-region (conn--region-beginning) (conn--region-end))
      (delete-region (point) (1+ (point))))
    (insert char)))

(defun conn-yank-replace (start end)
  "`yank' replacing region between START and END.

If called interactively uses the region between point and mark."
  (interactive (list (conn--region-beginning)
                     (conn--region-end)))
  (delete-region start end)
  (conn-call-binding conn-yank-keys))

(defun conn-transpose-things-at-pts (thing1 &optional thing2)
  "Transpose THING1 at `point' and THING2 at `mark'."
  (interactive
   (let ((things (conn--things 'conn--defined-thing-p)))
     (list (intern (completing-read "Thing: " things))
           (when current-prefix-arg
             (intern (completing-read "Second Thing: " things))))))
  (save-excursion
    (pcase-let
        ((`(,beg1 . ,end1) (bounds-of-thing-at-point thing1))
         (`(,beg2 . ,end2) (progn
                             (goto-char (mark t))
                             (bounds-of-thing-at-point (or thing2 thing1)))))
      (unless (or (null beg1) (null beg2) (= beg1 beg2))
        (transpose-regions beg1 end1 beg2 end2)))))

(defun conn-isearch-exit-and-mark ()
  "`isearch-exit' and set region to match."
  (interactive)
  (isearch-exit)
  (if isearch-forward
      (conn--push-ephemeral-mark isearch-other-end)
    (conn--push-ephemeral-mark (point))
    (goto-char isearch-other-end)))

(defun conn-isearch-exit-end ()
  "`isearch-exit' and leave point at end of match."
  (interactive)
  (isearch-exit)
  (unless isearch-forward
    (goto-char isearch-other-end)))

(defun conn-isearch-exit-beginning ()
  "`isearch-exit' and leave point at beginning of match."
  (interactive)
  (isearch-exit)
  (when isearch-forward
    (goto-char isearch-other-end)))

(defun conn-isearch-region (start end &optional regexp backwards)
  "Function `isearch-forward' for text in region START to END.

`isearch-backward' if BACKWARDS is non-nil.

Do a regexp search if REGEXP is non-nil.
`regexp-quote' region string if regexp is negative."
  (interactive (list (conn--region-beginning)
                     (conn--region-end)
                     current-prefix-arg
                     nil))
  (let* ((str (buffer-substring-no-properties start end)))
    (when (and regexp (< (prefix-numeric-value regexp) 0))
      (setq str (regexp-quote str)))
    (if backwards (goto-char end) (goto-char start))
    (isearch-mode (not backwards) regexp)
    (isearch-yank-string str)))

(defun conn-isearch-backward-region (start end &optional regexp)
  "`isearch-backward' for text in region from START to END.

Do a regexp search if REGEXP is non-nil.
`regexp-quote' region string if regexp is negative."
  (interactive (list (conn--region-beginning)
                     (conn--region-end)
                     current-prefix-arg))
  (conn-isearch-region start end regexp t))

(defun conn-open-line-and-indent (arg)
  "Insert ARG newlines and indent according to mode.
Moves point to new line."
  (interactive "p")
  (open-line arg)
  (save-excursion
    (forward-line arg)
    (indent-according-to-mode)))

(defun conn-end-of-inner-line (&optional N)
  (interactive "p")
  (unless N (setq N 1))
  (let ((pt (point)))
    (end-of-line (if (< N 0) (1+ N) N))
    (while (and (not (eobp))
                (and (eolp) (bolp)))
      (end-of-line (if (< N 0) 0 2)))
    (when-let ((cs (and (conn--point-is-in-comment-p)
                        (save-excursion
                          (comment-search-backward
                           (line-beginning-position) t)))))
      (goto-char cs)
      (skip-chars-backward " \t" (line-beginning-position))
      (when (bolp)
        (skip-chars-forward " \t" (line-end-position))))
    (when (and (called-interactively-p 'any)
               (= pt (point)))
      (forward-line)
      (conn-end-of-inner-line))))
(put 'inner-line 'end-op 'conn-end-of-inner-line)
(put 'inner-line 'forward-op 'conn-end-of-inner-line)

(defun conn-beginning-of-inner-line (&optional N)
  "Go to first non-whitespace character in line."
  (interactive "p")
  (unless N (setq N 1))
  (let ((pt (point)))
    (back-to-indentation)
    (unless (and (called-interactively-p 'any)
                 (= pt (point)))
      (cl-decf N (cl-signum N)))
    (when (> N 0)
      (setq pt (point))
      (forward-line (- N))
      (back-to-indentation)
      (while (= (pos-eol) (pos-bol))
        (forward-line (- (cl-signum N)))
        (back-to-indentation))
      (back-to-indentation))))
(put 'inner-line 'beginning-op 'conn-beginning-of-inner-line)

(defun conn-xref-definition-prompt ()
  "`xref-find-definitions' but always prompt."
  (interactive)
  (let ((current-prefix-arg t))
    (call-interactively #'xref-find-definitions)))

;; register-load from consult
(defun conn-register-load (reg &optional arg)
  "Do what I mean with a REG.

For a window configuration, restore it.  For a number or text, insert it.
For a location, jump to it.  See `jump-to-register' and `insert-register'
for the meaning of prefix ARG."
  (interactive
   (list
    (register-read-with-preview "Load register: ")
    current-prefix-arg))
  (condition-case err
      (jump-to-register reg arg)
    (user-error
     (unless (string-search "access aborted" (error-message-string err))
       (insert-register reg (not arg))))))

(defun conn-clear-register (register)
  "Reset REGISTER value."
  (interactive (list (register-read-with-preview "Clear register: ")))
  (set-register register nil))

(defun conn-yank ()
  (interactive)
  (conn-call-binding conn-yank-keys))

(defun conn-copy-region (start end &optional register)
  "Copy region between START and END as kill.

If REGISTER is given copy to REGISTER instead."
  (interactive (list (conn--region-beginning)
                     (conn--region-end)
                     (when current-prefix-arg
                       (register-read-with-preview "Copy to register: "))))
  (if register
      (if rectangle-mark-mode
          (copy-rectangle-to-register register start end)
        (copy-to-register register start end))
    (if rectangle-mark-mode
        (copy-rectangle-as-kill start end)
      (copy-region-as-kill start end)))
  (pulse-momentary-highlight-region start end 'region))

(defun conn-kill-region (&optional arg)
  "Kill region between START and END.

If START and END are equal delete char backward.

If ARG is an ordinary prefix argument (\\[universal-argument]) delete
the region instead of killing it.

If ARG is a numeric prefix argument kill region to a register."
  (interactive (list current-prefix-arg))
  (let ((mark-even-if-inactive t))
    (cond ((= (point) (mark t))
           (conn-call-binding conn-backward-delete-keys))
          ((consp arg)
           (let (kill-ring)
             (conn-call-binding conn-kill-region-keys)))
          ((numberp arg)
           (thread-first
             (concat "Kill "
                     (if rectangle-mark-mode "Rectangle " " ")
                     "to register:")
             (register-read-with-preview)
             (copy-to-register nil nil t t)))
          (t (conn-call-binding conn-kill-region-keys)))))

(defun conn-completing-yank-replace ()
  "Replace region from START to END with result of `yank-from-kill-ring'."
  (interactive)
  (let* ((yank-transform-functions (cons #'yank-in-context--transform
                                         yank-transform-functions)))
    (delete-region (conn--region-beginning) (conn--region-end))
    (call-interactively #'yank-from-kill-ring)))

(defun conn-yank-from-kill-ring-in-context ()
  (interactive)
  (let ((yank-transform-functions (cons #'yank-in-context--transform
                                        yank-transform-functions)))
    (call-interactively #'yank-from-kill-ring)))

(defun conn--uniquify-tab-name (name)
  (let ((tabs (seq-filter (lambda (tab-name)
                            (string-match (concat (regexp-quote name)
                                                  "\\( <[0-9]+>\\)?")
                                          tab-name))
                          (mapcar (lambda (tab-name)
                                    (alist-get 'name tab-name))
                                  (funcall tab-bar-tabs-function (selected-frame))))))
    (if tabs (concat name " <" (number-to-string (length tabs)) ">") name)))

(defun conn-tab-bar-new-named-tab (&optional name)
  (interactive
   (list (when current-prefix-arg
           (conn--uniquify-tab-name (read-from-minibuffer "Tab name: ")))))
  (tab-bar-new-tab-to)
  (when name (tab-rename name)))

(defun conn-tab-bar-duplicate-and-name-tab (&optional name)
  (interactive
   (list (when current-prefix-arg
           (conn--uniquify-tab-name (read-from-minibuffer "Tab name: ")))))
  (let ((tab-bar-new-tab-choice 'clone)
        (tab-bar-new-tab-group t))
    (tab-bar-new-tab-to)
    (when name (tab-rename name))))

(defun conn-indent-region ()
  (interactive)
  (let ((mark-even-if-inactive t))
    (call-interactively 'indent-region)))

;; from crux
(defun conn-duplicate-region (beg end &optional arg)
  "Duplicates the current line or region ARG times.
If there's no region, the current line will be duplicated.  However, if
there's a region, all lines that region covers will be duplicated."
  (interactive
   (let ((mark-even-if-inactive t))
     (list (conn--region-beginning)
           (conn--region-end)
           (prefix-numeric-value current-prefix-arg))))
  (let* ((origin (point))
         (region (buffer-substring-no-properties beg end)))
    (dotimes (_i arg)
      (goto-char end)
      (push-mark nil nil t)
      (insert region)
      (setq end (point)))
    (goto-char (+ origin (* (length region) arg)))))

(defun conn-duplicate-and-comment-region (beg end &optional arg)
  "Duplicates the current line or region ARG times.
If there's no region, the current line will be duplicated.  However, if
there's a region, all lines that region covers will be duplicated."
  (interactive
   (let ((mark-even-if-inactive t))
     (list (conn--region-beginning)
           (conn--region-end)
           (prefix-numeric-value current-prefix-arg))))
  (let* ((origin (point))
         (oend end)
         (region (buffer-substring-no-properties beg end)))
    (dotimes (_i arg)
      (goto-char end)
      (insert region)
      (setq end (point)))
    (goto-char (+ origin (* (length region) arg)))
    (comment-region beg oend)))

(cl-macrolet
    ((transpose-backward (&body fns)
       `(progn
          ,@(cl-loop for transpose-fn in fns
                     for name = (intern (concat "conn-"
                                                (symbol-name transpose-fn)
                                                "-backward"))
                     nconc `((defun ,name (arg)
                               (interactive "p")
                               (,transpose-fn (- arg)))
                             (conn-set-repeat-command ',name))))))
  (transpose-backward transpose-words
                      transpose-sexps
                      transpose-lines
                      transpose-paragraphs
                      transpose-chars))

(define-minor-mode conn-buffer-dedicated-mode
  "Minor mode for dedicating windows."
  :init-value nil
  :lighter " [D]"
  (if (set-window-dedicated-p
       (selected-window)
       (unless (window-dedicated-p (selected-window))
         'weak))
      (message "Window dedicated.")
    (message "Window no longer dedicated.")))

(provide 'conn-core)
;;; conn-core.el ends here
