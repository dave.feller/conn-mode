;;; conn-mode.el --- Modal keybinding mode -*- lexical-binding: t -*-
;;
;; Filename: conn-mode.el
;; Description: A modal keybinding mode and keyboard macro enhancement
;; Author: David Feller
;; Package-Version: 0.1
;; Package-Requires: ((emacs "28.1") (compat "29.1.4.1"))
;;
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 51 Franklin Street, Fifth
;; Floor, Boston, MA 02110-1301, USA.
;;
;;; Commentary:
;;
;; Modal keybinding mode
;;
;;; Code:

(require 'pulse)
(require 'conn-utils)
(require 'conn-core)
(require 'conn-dots)
(require 'conn-mark)
(require 'conn-keymaps)
(require 'face-remap)

(defun conn--initialize-buffer ()
  "Initialize conn STATE in BUFFER."
  (conn-local-mode 1))

(define-minor-mode conn-local-mode
  "Minor mode for setting up conn in a buffer"
  :init-value nil
  :keymap conn-mode-map
  :lighter (:eval conn-lighter)
  (if conn-local-mode
      (progn
        (setq buffer-face-mode-face 'default)
        (unless (mark t)
          (conn--push-ephemeral-mark (point) t nil))
        (add-hook 'pre-command-hook #'conn--mark-trail-pre-command-hook nil t)
        (add-hook 'post-command-hook #'conn--thing-post-command-hook nil t)
        (add-hook 'change-major-mode-hook #'conn--clear-overlays nil t)
        (add-hook 'input-method-activate-hook #'conn--activate-input-method nil t)
        (add-hook 'input-method-deactivate-hook #'conn--deactivate-input-method nil t)
        (add-hook 'clone-indirect-buffer-hook 'conn--reset-mark-cursor nil t)
        (setq conn--input-method current-input-method)
        (funcall (conn--default-state-for-buffer (current-buffer)))
        (setq conn--hide-mark-cursor (conn--hide-mark-cursor-p))
        (conn--refresh-cursor))
    (conn-dot-movement-mode -1)
    (conn--clear-dots)
    (when conn-current-state
      (funcall (get conn-current-state :conn-transition-fn) t))
    (setq conn-current-state nil)
    (when conn--mark-cursor
      (delete-overlay conn--mark-cursor)
      (setq-local conn--mark-cursor nil))
    (remove-hook 'pre-command-hook #'conn--mark-trail-pre-command-hook t)
    (remove-hook 'post-command-hook #'conn--thing-post-command-hook t)
    (remove-hook 'change-major-mode-hook #'conn--clear-overlays t)
    (remove-hook 'input-method-activate-hook #'conn--activate-input-method t)
    (remove-hook 'input-method-deactivate-hook #'conn--deactivate-input-method t)
    (remove-hook 'clone-indirect-buffer-hook 'conn--reset-mark-cursor t)))

;;;###autoload
(define-globalized-minor-mode conn-mode
  conn-local-mode conn--initialize-buffer
  :group 'conn-mode
  :predicate t
  (progn
    (conn--setup-keymaps)
    (conn--setup-mark)
    (conn--setup-advice)
    (if conn-mode
        (progn
          (add-hook 'window-configuration-change-hook #'conn--refresh-cursor)
          (when conn-mode-line-indicator
            (set-default
             'mode-line-format
             `((conn-mode
                (:eval conn--mode-line-format))
               ,@(assq-delete-all
                  'conn-mode
                  (default-value 'mode-line-format)))))
          (force-mode-line-update))
      (remove-hook 'window-configuration-change-hook #'conn--refresh-cursor)
      (set-default
       'mode-line-format
       `(,@(assq-delete-all
            'conn-mode
            (default-value 'mode-line-format))))
      (force-mode-line-update))))

(provide 'conn-mode)
;;; conn-mode.el ends here
