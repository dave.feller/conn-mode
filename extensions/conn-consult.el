;;; conn-consult.el --- conn-mode file is not part of GNU Emacs.

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 3
;; of the License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
;; Boston, MA 02110-1301, USA.

;;; Commentary:

;;; Consult integration for conn-mode

;;; Code:

(require 'conn-mode)
(require 'consult)

(defmacro conn--each-thing (thing beg end &rest body)
  "Iterate over each line.

The line beginning/ending BEG/END is bound in BODY."
  (declare (indent 3))
  (cl-with-gensyms (max bounds)
    `(save-excursion
       (let ((,max (point-max)))
         (goto-char (point-min))
         (unless (bounds-of-thing-at-point ,thing)
           (forward-thing ,thing 1))
         (while (< (point) ,max)
           (let* ((,bounds (bounds-of-thing-at-point ,thing))
                  (,beg (car ,bounds))
                  (,end (cdr ,bounds)))
             ,@body
             (forward-thing ,thing 1)))))))

(defun conn--thing-candidates (thing)
  "Return list of thing candidates."
  (consult--forbid-minibuffer)
  (consult--fontify-all)
  (let* ((buffer (current-buffer))
         default-cand candidates)
    (conn--each-thing thing beg end
      (let ((line (line-number-at-pos)))
        (push (consult--location-candidate
               (consult--buffer-substring beg end)
               (cons buffer beg) line line)
              candidates))
      (when (not default-cand)
        (setq default-cand candidates)))
    (unless candidates
      (user-error "No lines"))
    (nreverse candidates)))

;;;###autoload
(defun conn-consult-thing (&optional initial thing start)
  "Search for a matching top-level THING."
  (interactive (list nil
                     (intern
                      (completing-read
                       (format "Thing: ")
                       (conn--things 'conn--defined-thing-p) nil nil nil
                       'conn-thing-history))
                     (not (not current-prefix-arg))))
  (let* ((candidates (consult--slow-operation "Collecting things..."
                       (conn--thing-candidates thing))))
    (consult--read
     candidates
     :prompt "Goto thing: "
     :annotate (consult--line-prefix)
     :category 'consult-location
     :sort nil
     :require-match t
     ;; Always add last isearch string to future history
     :add-history (list (thing-at-point 'symbol) isearch-string)
     ;; :history '(:input consult--line-history)
     :lookup #'consult--line-match
     :default (car candidates)
     ;; Add isearch-string as initial input if starting from isearch
     :initial (or initial
                  (and isearch-mode
                       (prog1 isearch-string (isearch-done))))
     :state (consult--location-state candidates))))

(defun conn-dot-consult-line-candidate (cand)
  (let ((marker (car (consult--get-location cand))))
    (with-current-buffer (marker-buffer marker)
      (goto-char marker)
      (unless (bolp) (beginning-of-line))
      (conn--create-dots (cons (point) (progn (end-of-line) (point)))))))

(defun conn-dot-consult-grep-candidate (cand)
  (let ((marker (car (consult--grep-position cand))))
    (with-current-buffer (marker-buffer marker)
      (goto-char marker)
      (unless (bolp) (beginning-of-line))
      (conn--create-dots (cons (point) (progn (end-of-line) (point)))))))

(provide 'conn-consult)
;;; conn-consult.el ends here
