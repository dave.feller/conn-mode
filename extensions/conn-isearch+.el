;;; conn-isearch+.el --- conn-mode file is not part of GNU Emacs.

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 3
;; of the License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
;; Boston, MA 02110-1301, USA.

;;; Commentary:

;;; Isearch+ integration for conn-mode.

;;; Code:

(require 'conn-mode)
(require 'isearch+)

(cl-pushnew '("~[dot]" conn-isearch-not-in-dot-p "~[DOT]")
            isearchp-current-filter-preds-alist
            :test #'equal)

(cl-pushnew '( "[dot]" conn-isearch-in-dot-p      "[DOT]")
            isearchp-current-filter-preds-alist
            :test #'equal)

(defun conn-isearch-in-dot-toggle ()
  "Toggle restricting search to dots."
  (interactive)
  (if (advice-function-member-p #'conn-isearch-in-dot-p isearch-filter-predicate)
      (isearchp-remove-filter-predicate "conn-isearch-in-dot-p")
    (isearchp-add-filter-predicate '("[dot]" conn-isearch-in-dot-p "[DOT]"))))

(defun conn--isearch-in-dots-hook ()
  (isearchp-add-filter-predicate '("[dot]" conn-isearch-in-dot-p "[DOT]")))

(define-keymap
  :keymap isearch-mode-map
  "M-," 'conn-isearch-in-dot-toggle)

(provide 'conn-isearch+)
;;; conn-isearch+.el ends here
