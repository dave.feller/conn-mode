;;; conn-embark.el --- this file is not part of GNU Emacs. -*- lexical-binding: t -*-
;;
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.
;;
;;; Commentary:
;;
;; embark integration for conn-mode
;;
;;; Code:

(require 'embark)
(require 'conn-core)

(defcustom conn-complete-keys-bindings (list "M-S-<iso-lefttab>")
  "List of keybindings for to try binding `conn-complete-keys' to when
`conn-complete-keys-mode' is enabled. Keys are only bound to `conn-complete-keys'
when they are not already bound."
  :type '(list string)
  :group 'conn-complete-keys)

(defcustom conn-complete-keys-prefix-help-command nil
  "If non-nil use `conn-complete-keys' as `prefix-help-command'."
  :type 'boolean
  :group 'conn-complete-keys)

(defun conn-complete-keys--get-bindings (prefix map)
  (let ((prefix-map (if (= 0 (seq-length prefix))
                        map
                      (keymap-lookup map (key-description prefix))))
        binds)
    (cond
     ((or (null prefix-map) (numberp prefix-map)))
     ((keymapp prefix-map)
      (map-keymap
       (lambda (key def)
         (cond
          ((and (numberp key)
                (= key 27)
                (keymapp def))
           (map-keymap
            (lambda (key2 def2)
              (unless (memq def (list 'undefined 'self-insert-command 'digit-argument
                                      'negative-argument 'embark-keymap-help nil))
                (push (cons (vconcat (vector key key2)) def2) binds)))
            def))
          (t (push (cons (vector key) def) binds))))
       (keymap-canonicalize prefix-map))))
    (nreverse binds)))

;; `embark--formatted-bindings' almost
(defun conn-complete-keys--formatted-bindings (map)
  "Return the formatted keybinding of KEYMAP.
The keybindings are returned in their order of appearance.
If NESTED is non-nil subkeymaps are not flattened."
  (let* ((commands
          (cl-loop for (key . def) in map
                   for name = (embark--command-name def)
                   for cmd = (keymap--menu-item-binding def)
                   unless (memq cmd '(nil embark-keymap-help
                                          negative-argument
                                          digit-argument
                                          self-insert-command
                                          undefined))
                   collect (list name cmd key
                                 (concat
                                  (if (eq (car-safe def) 'menu-item)
                                      "menu-item"
                                    (key-description key))))))
         (width (cl-loop for (_name _cmd _key desc) in commands
                         maximize (length desc)))
         (default)
         (candidates
          (cl-loop for item in commands
                   for (name cmd key desc) = item
                   for desc-rep =
                   (concat
                    (propertize desc 'face 'embark-keybinding)
                    (and (embark--action-repeatable-p cmd)
                         embark-keybinding-repeat))
                   for formatted =
                   (propertize
                    (concat desc-rep
                            (make-string (- width (length desc-rep) -1) ?\s)
                            name)
                    'embark-command cmd)
                   ;; when (equal key [13]) do (setq default formatted)
                   collect (cons formatted item))))
    candidates))

;;;###autoload
(defun conn-complete-keys (prefix map)
  "Complete key sequence beginning with current prefix keys using `completing-read'."
  (interactive
   (let* ((prefix (seq-subseq (this-command-keys-vector) 0 -1)))
     (list prefix (make-composed-keymap (current-active-maps t)))))
  (let* ((up (lambda ()
               (interactive)
               (delete-minibuffer-contents)
               (exit-minibuffer)))
         (tree (lambda ()
                 (interactive)
                 (embark--quit-and-run
                  (lambda ()
                    (conn-complete-keys prefix map)))))
         (flat (lambda ()
                 (interactive)
                 (embark--quit-and-run
                  (lambda ()
                    (minibuffer-with-setup-hook
                      (lambda ()
                        (message "test")
                        (use-local-map
                         (define-keymap
                           :parent (current-local-map)
                           "M-T" tree)))
                      (embark-bindings-in-keymap
                       (keymap-lookup map (key-description prefix))))))))
         prompt choice cand)
    (cl-loop
     (setq cand (conn-complete-keys--formatted-bindings
                 (conn-complete-keys--get-bindings prefix map))
           prompt (if (> (length prefix) 0)
                      (concat "Key: " (key-description prefix) "- ")
                    "Key: ")
           choice (minibuffer-with-setup-hook
                      (:append
                       (lambda ()
                         (use-local-map
                          (define-keymap
                            :parent (current-local-map)
                            "M-<backspace>" up
                            "M-DEL" up
                            "M-F" flat))))
                    (completing-read
                     prompt
                     (lambda (string predicate action)
                       (if (eq action 'metadata)
                           `(metadata (display-sort-function . ,(lambda (c) (sort c 'string<)))
                                      (category . embark-keybinding))
                         (complete-with-action action cand string predicate)))
                     nil nil)))
     (pcase (assoc choice cand)
       ('nil
        (if (not (string= choice ""))
            (cl-return)
          (setq prefix (seq-subseq prefix 0 -1))
          (when (and (> (length prefix) 0)
                     (= 27 (elt prefix (1- (length prefix)))))
            ;; This was a meta bind so we need to
            ;; remove the ESC key as well
            (setq prefix (seq-subseq prefix 0 -1)))))
       (`(,_ ,_ ,cmd ,key ,_)
        (if (keymapp cmd)
            (setq prefix (vconcat prefix key))
          (cl-return (call-interactively cmd))))))))

;;;###autoload
(define-minor-mode conn-complete-keys-mode
  "Minor mode for key completion."
  :global t
  :lighter ""
  ;; conn-complete-keys-local-mode conn-complete-keys--init
  :group 'conn-complete-keys
  (if conn-complete-keys-mode
      (progn
        (dolist (key conn-complete-keys-bindings)
          (unless (keymap-lookup global-map key)
            (keymap-global-set key 'conn-complete-keys)))
        (when conn-complete-keys-prefix-help-command
          (setq conn-complete-keys--prefix-cmd-backup prefix-help-command)
          (setq prefix-help-command 'conn-complete-keys)))
    (dolist (key conn-complete-keys-bindings)
      (when (eq 'conn-complete-keys (keymap-global-lookup key))
        (keymap-global-unset key t)))
    (when (eq prefix-help-command 'conn-complete-keys)
      (setq prefix-help-command conn-complete-keys--prefix-cmd-backup))))

(defvar-keymap conn-region-case-map
  :prefix 'conn-region-case-map
  "-" 'conn-kebab-case-region
  "c" 'capitalize-region
  "d" 'downcase-region
  "u" 'upcase-region
  "o" 'conn-capital-case-region
  "a" 'conn-camel-case-region
  "S" 'conn-capital-snake-case-region
  "s" 'conn-snake-case-region)

(define-keymap
  :keymap embark-region-map
  "C-s" 'conn-isearch-region
  "C-r" 'conn-isearch-backward-region
  "c" 'conn-region-case-map
  "(" 'conn-insert-pair
  ")" 'conn-change-pair
  "[" 'conn-delete-pair
  "]" 'conn-collapse-whitespace-pair)

(add-to-list 'embark-target-injection-hooks
             '(conn-insert-pair embark--ignore-target))
(add-to-list 'embark-target-injection-hooks
             '(conn-change-pair embark--ignore-target))

(defun conn-replace-region-substring (from-string)
  (let ((to-string (read-string "Replace with: ")))
    (save-excursion
      (goto-char (point-min))
      (while (re-search-forward from-string nil t)
        (replace-match to-string nil nil)))))

(defun conn--embark-target-region ()
  (let ((start (conn--region-beginning))
        (end (conn--region-end)))
    `(region ,(buffer-substring start end) . (,start . ,end))))

;;;###autoload
(defun conn-embark-region ()
  (interactive)
  (let* ((mark-even-if-inactive t)
         (embark-target-finders
             (cons 'conn--embark-target-region
                   (remq 'embark-target-active-region
                         embark-target-finders))))
    (embark-act)))

;;;###autoload
(defun conn-embark-replace-region (string)
  (interactive (list (read-string "Replace with: ")))
  (delete-region (conn--region-beginning) (conn--region-end))
  (insert string))

(provide 'conn-embark)
;;; conn-embark.el ends here
