;;; conn-utils.el --- Utility functions for conn-mode  -*- lexical-binding: t; -*-
;;
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.
;;
;;; Commentary:
;;
;; Utility functions for conn-mode.
;;
;;; Code:

(defun conn--rotate (N list)
  (let ((len (length list)))
    (nconc (seq-subseq list (mod (- N) len))
           (seq-subseq list 0 (mod (- N) len)))))

;; From repeat-mode
(defun conn--command-property (property)
  (or (and (symbolp this-command)
           (get this-command property))
      (and (symbolp real-this-command)
           (get real-this-command property))))

;; From expand-region
(defun conn--point-is-in-comment-p ()
  "t if point is in comment, otherwise nil"
  (or (nth 4 (syntax-ppss))
      (memq (get-text-property (point) 'face)
            '(font-lock-comment-face font-lock-comment-delimiter-face))))

;; From misearch
(defun conn-read-matching-dot-buffers ()
  "Return a list of buffers whose names match specified regexp.
Uses `read-regexp' to read the regexp."
  ;; Most code from `multi-occur-in-matching-buffers'
  ;; and `kill-matching-buffers'.
  (let ((bufregexp
         (read-regexp "Search in buffers whose names match regexp")))
    (when bufregexp
      (delq nil (mapcar (lambda (buf)
                          (when (string-match bufregexp (buffer-name buf))
                            buf))
                        (buffer-list))))))

;; From misearch
(defun conn-read-dot-buffers ()
  "Return a list of buffers specified interactively, one by one."
  ;; Most code from `multi-occur'.
  (let* ((collection (mapcar #'buffer-name
                             (seq-filter #'conn--dots-active-p (buffer-list)))))
    (completing-read-multiple "First buffer: " collection nil t)))

;;; From thingatpt+
(defun conn--defined-thing-p (thing)
  (when (consp thing) (setq thing  (car thing)))
  (when (stringp thing) (setq thing  (intern thing)))
  (let ((forward-op    (or (get thing 'forward-op)  (intern-soft (format "forward-%s" thing))))
        (beginning-op  (get thing 'beginning-op))
        (end-op        (get thing 'end-op))
        (bounds-fn     (get thing 'bounds-of-thing-at-point))
        (thing-fn      (get thing 'thing-at-point)))
    (or (functionp forward-op)
        (and (functionp beginning-op)  (functionp end-op))
        (functionp bounds-fn)
        (functionp thing-fn))))

(defun conn--movement-thing-p (thing)
  (when (consp thing) (setq thing  (car thing)))
  (when (stringp thing) (setq thing  (intern thing)))
  (or (get thing 'forward-op)  (intern-soft (format "forward-%s" thing))))

;;; From thingatpt+
(defun conn--things (predicate)
  (let (types)
    (mapatoms
     (lambda (tt)
       (when (funcall predicate tt) (push (symbol-name tt) types))))
    (dolist (typ  '("thing" "buffer" "point")) ; Remove types that do not make sense.
      (setq types (delete typ types)))
    (sort types #'string-lessp)))

(defun conn--minimize (fn list)
  (let ((d (funcall fn (car list)))
        (result (car list)))
    (dolist (e (cdr list))
      (let ((d2 (funcall fn e)))
        (when (< d2 d)
          (setq d d2
                result e))))
    result))

(defun conn--beginning-of-region-or-restriction ()
  (if (use-region-p) (region-beginning) (point-min)))

(defun conn--end-of-region-or-restriction ()
  (if (use-region-p) (region-end) (point-max)))

(defun conn--symbolicate (&rest symbols-or-strings)
  "Concatenate all SYMBOLS-OR-STRINGS to create a new symbol."
  (let ((name (mapcar (lambda (e)
                        (cl-etypecase e
                          (string e)
                          (symbol (symbol-name e))))
                      symbols-or-strings)))
    (intern (apply #'concat name))))

(defun conn--create-marker (pos &optional buffer)
  "Create marker at POS in BUFFER."
  (let ((marker (make-marker)))
    (set-marker marker pos buffer)
    marker))

;; From meow
(defmacro conn--as-merged-undo (&rest body)
  "Execute BODY as a single undo unit."
  (declare (indent defun))
  (let ((handle (gensym "change-group-handle"))
        (success (gensym "change-group-success")))
    `(let ((,handle (prepare-change-group))
           (undo-outer-limit nil)
           (undo-limit most-positive-fixnum)
           (undo-strong-limit most-positive-fixnum)
           (,success nil))
       (unwind-protect
           (prog2
               (activate-change-group ,handle)
               ,(macroexp-progn body)
             (setq ,success t))
         (if (not ,success)
             (cancel-change-group ,handle)
           (accept-change-group ,handle)
           (undo-amalgamate-change-group ,handle))))))

(provide 'conn-utils)
;;; conn-utils.el ends here
