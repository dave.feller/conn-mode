;;; conn-dots.el --- Keyboard macro enhancements for conn  -*- lexical-binding: t; -*-
;;
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.
;;
;;; Commentary:
;;
;; keyboard macro enhancements.
;;
;;; Code:

;;;; Requires

(require 'compat)
(require 'pulse)
(require 'conn-core)
(require 'conn-utils)

;;;; Variables
;;;;; Custom Variables

(defface conn-dot-face
  '((t (:background "#f0c1cf")))
  "Face for dots."
  :group 'conn-mode)

(defcustom conn-overlay-priority 1001
  "Priority of dot overlays."
  :type 'integer
  :group 'conn-mode)

(defcustom conn-last-dot-macro-register ?.
  "Register used for the last dot macro."
  :type 'character
  :group 'conn-mode)

;;;;; internal variables

(defvar conn--macro-dispatch-p nil
  "Non-nil when a keyboard is being executed on dots.")

(defvar-local conn--dot-undoing nil)
(defvar-local conn--dot-undo-ring nil)
(defvar-local conn--dot-undone nil)
(defvar-local conn--dot-this-undo nil)

(defvar conn-dot-undo-ring-max 32
  "Maximum size of the dot undo ring.")

;;;; Dot internal
;;;;; Dot registers

(cl-defstruct (conn-dot-register (:constructor %conn-make-dot-register (data)))
  (data nil :read-only t))

(defun conn-make-dot-register ()
  (let ((buffers (mapcar #'get-buffer (conn-read-dot-buffers)))
        dots curr)
    (dolist (buf buffers)
      (setq curr (list buf))
      (with-current-buffer buf
        (conn--for-each-dot
         (lambda (dot)
           (push (cons (conn--create-marker (overlay-start dot) buf)
                       (conn--create-marker (overlay-end dot) buf))
                 (cdr curr)))))
      (push curr dots))
    (%conn-make-dot-register dots)))

(cl-defmethod register-val-jump-to ((val conn-dot-register) _arg)
  (pcase-dolist (`(,buf . ,dots) (conn-dot-register-data val))
    (with-current-buffer buf
      (save-restriction
        (widen)
        (conn--clear-dots (point-min) (point-max))
        (apply #'conn--create-dots dots)))))

(cl-defmethod register-val-describe ((val conn-dot-register) _arg)
  (princ (format "dot state in buffers:\n   %s"
                 (mapcar (lambda (buf)
                           (buffer-name (car buf)))
                         (conn-dot-register-data val)))))

(defun conn-dot-state-to-register (register)
  "Store currently marked dots in REGISTER."
  (interactive (list (register-read-with-preview "Dot state to register: ")))
  (set-register register (conn-make-dot-register)))

(cl-defstruct (conn-dot-macro-register)
  (macro nil :read-only t)
  (setup-fn nil :read-only t)
  (cleanup-fn nil :read-only t))

(cl-defmethod register-val-jump-to ((val conn-dot-macro-register) arg)
  (let ((dots (save-mark-and-excursion (conn-first-dot))))
    (unless dots
      (if (use-region-p)
          (apply #'conn--create-dots (region-bounds))
        (conn-dot-point (point))))
    (conn--macro-dispatch (if arg (conn-read-dot-buffers) (list (current-buffer)))
                          :setup-fn (conn-dot-macro-register-setup-fn val)
                          :cleanup-fn (conn-dot-macro-register-cleanup-fn val)
                          :macro (conn-dot-macro-register-macro val))
    (unless dots (conn--clear-dots))))

(cl-defmethod register-val-describe ((val conn-dot-macro-register) _arg)
  (princ (format "Dot Macro:\n   %s"
                 (key-description (conn-dot-macro-register-macro val)))))

;;;;; Dot functions

(defun conn--clear-overlays ()
  "Delete all conn overlays."
  (dolist (ov (flatten-tree (overlay-lists)))
    (when (overlay-get ov 'conn-overlay)
      (delete-overlay ov))))

(defun conn--dot-post-command-hook ()
  (when conn--dot-this-undo
    (setq conn--dot-undone nil)
    (push conn--dot-this-undo conn--dot-undo-ring)
    (when (> (length conn--dot-undo-ring)
             conn-dot-undo-ring-max)
      (setq conn--dot-undo-ring
            (seq-take conn--dot-undo-ring conn-dot-undo-ring-max)))
    (setq conn--dot-this-undo nil)))

(defun conn--dot-at-point (point)
  (or (seq-find #'conn-dotp (overlays-at point))
      (seq-find #'conn-dotp (overlays-at (1- point)))))

(defun conn--for-each-dot (func &optional sort-predicate start end)
  "Apply FUNC to each dot.
Optionally between START and END and sorted by SORT-PREDICATE."
  (when-let ((dots (if sort-predicate
                       (conn--sorted-overlays #'conn-dotp sort-predicate start end)
                     (conn--all-overlays #'conn-dotp start end))))
    (mapc func dots)))

(defun conn--move-each-dot (func)
  "Move each dot to the value of `region-beginning' and `region-end'
after applying FUNC."
  (let (transforms)
    (save-mark-and-excursion
      (conn--for-each-dot
       (lambda (dot)
         (funcall func dot)
         (when (and transforms (>= (nth 2 (car transforms))
                                   (conn--region-beginning)))
           (user-error "Overlapping regions"))
         (push (list dot (conn--region-beginning) (conn--region-end)) transforms))
       '<))
    (dolist (tform transforms)
      (apply #'conn--move-dot tform))))

(defun conn--move-dot (dot start end)
  (let ((old-start (overlay-start dot))
        (old-end (overlay-end dot)))
    (move-overlay dot start end)
    (unless (or conn--dot-undoing
                conn--macro-dispatch-p)
      (push `(move (,start . ,end) . (,old-start . ,old-end))
            conn--dot-this-undo))))

(defun conn--delete-dot (dot)
  (unless (or conn--dot-undoing
              conn--macro-dispatch-p)
    (push `(delete ,(overlay-start dot) . ,(overlay-end dot))
          conn--dot-this-undo))
  (overlay-put dot 'dot nil)
  (delete-overlay dot))

(defun conn--create-dots (&rest bounds)
  (pcase-dolist (`(,start . ,end) bounds)
    (with-current-buffer (if (markerp start)
                             (marker-buffer start)
                           (current-buffer))
      (let* ((overlaps (conn--all-overlays #'conn-dotp start end))
             (start (apply #'min start (mapcar #'overlay-start overlaps)))
             (end (apply #'max end (mapcar #'overlay-end overlaps)))
             (overlay (make-overlay start end nil nil t)))
        (mapc #'conn--delete-dot overlaps)
        (overlay-put overlay 'conn-overlay t)
        (overlay-put overlay 'dot t)
        (overlay-put overlay 'priority conn-overlay-priority)
        (overlay-put overlay 'face 'conn-dot-face)
        (overlay-put overlay 'evaporate t)
        (unless (or conn--dot-undoing
                    conn--macro-dispatch-p)
          (push `(create ,start . ,end) conn--dot-this-undo))
        overlay))))

(defun conn--clear-dots (&optional start end)
  (mapc #'conn--delete-dot
        (conn--all-overlays #'conn-dotp
                            (or start (conn--beginning-of-region-or-restriction))
                            (or end (conn--end-of-region-or-restriction)))))

(defun conn--all-overlays (predicate &optional start end)
  "Get all overlays between START and END satisfying PREDICATE."
  (seq-filter predicate
              (overlays-in (or start (conn--beginning-of-region-or-restriction))
                           (or end (conn--end-of-region-or-restriction)))))

(declare-function conn--sorted-overlays "conn-core")
(if (version<= "29" emacs-version)
    (cl-defun conn--sorted-overlays (typep &optional (predicate '<) start end)
      "Get all dots between START and END sorted by starting position."
      (let ((overlays (conn--all-overlays typep start end)))
        (pcase predicate
          ('< overlays)
          ('> (nreverse overlays))
          (_ (sort overlays predicate)))))

  (cl-defun conn--sorted-overlays (typep &optional (predicate '<) start end)
    "Get all dots between START and END sorted by starting position."
    (let ((overlays (conn--all-overlays typep start end)))
      (pcase predicate
        ('< (cl-sort overlays #'< :key #'overlay-start))
        ('> (cl-sort overlays #'> :key #'overlay-start))
        (_  (cl-sort overlays predicate))))))

(defun conn--replace-each-dot (strings dots)
  "Replaces contents of each dot in DOTS with corresponding string in STRINGS."
  (unless (= (length strings) (length dots))
    (error "STRINGS length does not match CONN--ACTIVE-DOTS length"))
  (save-excursion
    (while strings
      (let ((dot (pop dots)))
        (goto-char (overlay-start dot))
        (insert (pop strings))
        (delete-region (point) (overlay-end dot))))))

(defun conn-dotp (overlay)
  "Return t if OVERLAY is a dot."
  (overlay-get overlay 'dot))

(defun conn--dot-string (dot)
  "Return contents of DOT as a string."
  (buffer-substring-no-properties
   (overlay-start dot) (overlay-end dot)))

(defun conn--clear-dots-in-buffers (buffers)
  "Delete all dots in BUFFERS."
  (dolist (buf buffers)
    (with-current-buffer buf
      (save-restriction
        (widen)
        (conn--clear-dots)))))

(defun conn--dots-active-p (&optional buffer)
  (with-current-buffer (or buffer (current-buffer))
    (let (curr)
      (save-excursion
        (save-restriction
          (widen)
          (goto-char (point-min))
          (goto-char (next-overlay-change (point)))
          (while (and (/= (point) (point-max))
                      (not (setq curr (conn--dot-at-point (point)))))
            (goto-char (next-overlay-change (point))))
          curr)))))

(defmacro conn-with-saved-state (&rest body)
  "Execute BODY preserving current conn state and previous state values"
  (declare (indent defun))
  (let ((saved-state (gensym "saved-state"))
        (saved-previous-state (gensym "saved-previous-state")))
    `(let ((,saved-state conn-current-state)
           (,saved-previous-state conn-previous-state)
           (conn-previous-state conn-previous-state))
       (unwind-protect
           ,(macroexp-progn body)
         (funcall ,saved-state)
         (setq conn-previous-state ,saved-previous-state)))))

(cl-defgeneric conn--macro-dispatch (buffers &key setup-fn cleanup-fn &allow-other-keys))

(cl-defmethod conn--macro-dispatch ((buffers list) &rest rest
                                    &key setup-fn cleanup-fn macro &allow-other-keys)
  (let ((last-kbd-macro macro)
        (conn--macro-dispatch-p t)
        (wind (current-window-configuration)))
    (unwind-protect
        (progn
          (when (cl-notany 'conn--dots-active-p buffers)
            (error "No dots for dispatch"))
          (dolist (buf buffers)
            (apply 'conn--macro-dispatch buf rest))
          (set-register conn-last-dot-macro-register
                        (make-conn-dot-macro-register :macro last-kbd-macro
                                                      :setup-fn setup-fn
                                                      :cleanup-fn cleanup-fn)))
      (set-window-configuration wind))))

(cl-defmethod conn--macro-dispatch ((buf buffer) &context (last-kbd-macro (eql nil))
                                    &rest rest &key &allow-other-keys)
  (pop-to-buffer-same-window buf)
  (conn-with-saved-state
    (conn--as-merged-undo
      (save-mark-and-excursion
        (deactivate-mark t)
        (dolist (dot (let* ((dots (conn--sorted-overlays #'conn-dotp '>))
                            (first (conn--minimize
                                    (lambda (ov)
                                      (min (abs (- (overlay-start ov) (point)))
                                           (abs (- (overlay-end ov) (point)))))
                                    dots))
                            (rest (remq first dots)))
                       (cons first rest)))
          (apply 'conn--macro-dispatch dot rest))))))

(cl-defmethod conn--macro-dispatch ((buf buffer) &rest rest
                                    &key &allow-other-keys)
  (pop-to-buffer-same-window buf)
  (conn-with-saved-state
    (conn--as-merged-undo
      (save-mark-and-excursion
        (deactivate-mark t)
        (dolist (dot (conn--sorted-overlays #'conn-dotp '>))
          (apply 'conn--macro-dispatch dot rest))))))

(cl-defmethod conn--macro-dispatch ((dot overlay) &context (last-kbd-macro (eql nil))
                                    &key setup-fn cleanup-fn &allow-other-keys)
  (let ((mark-ring mark-ring))
    (unwind-protect
        (progn
          (overlay-put dot 'evaporate nil)
          (overlay-put dot 'face nil)
          (when setup-fn (funcall setup-fn dot))
          (kmacro-start-macro nil)
          (pulse-momentary-highlight-region (conn--region-beginning) (conn--region-end) 'conn-dot-face)
          (unwind-protect
              (recursive-edit)
            (kmacro-end-macro nil)))
      (conn--move-dot dot (conn--region-beginning) (conn--region-end))
      (when cleanup-fn (funcall cleanup-fn dot))
      (overlay-put dot 'evaporate t)
      (overlay-put dot 'face 'conn-dot-face))))

(cl-defmethod conn--macro-dispatch ((dot overlay) &key setup-fn cleanup-fn &allow-other-keys)
  (let ((mark-ring mark-ring))
    (with-demoted-errors "Error in macro dispatch: %s"
      (unwind-protect
          (progn
            (overlay-put dot 'evaporate nil)
            (when setup-fn (funcall setup-fn dot))
            (kmacro-call-macro nil nil nil last-kbd-macro))
        (conn--move-dot dot (conn--region-beginning) (conn--region-end))
        (when cleanup-fn (funcall cleanup-fn dot))
        (overlay-put dot 'evaporate t)))))

(defun conn--dot-after-movement ()
  (when-let ((thing (conn--command-property :conn-thing-movement-cmd)))
    (conn-dot-all-things-in-region thing)))

(define-minor-mode conn-dot-movement-mode
  "Dot after any thing movement command."
  :lighter (:propertize " Dot-M" face conn-dot-state-lighter-face)
  (if conn-dot-movement-mode
      (progn
        (add-hook 'post-command-hook 'conn--dot-after-movement 91 t)
        (when current-prefix-arg
          (apply #'conn--create-dots (conn--region-bounds))))
    (remove-hook 'post-command-hook 'conn--dot-after-movement t)))

;;;; Dot Commands
;;;;; Transitions

(defun conn-dot-quit ()
  (interactive)
  (conn--clear-dots)
  (conn-pop-state))

(defun conn-dots-command (&optional multi-buffer)
  (interactive "P")
  (conn--macro-dispatch
   (cond ((consp multi-buffer)
          (conn-read-matching-dot-buffers))
         (multi-buffer
          (conn-read-dot-buffers))
         (t (list (current-buffer))))
   :setup-fn (lambda (dot)
               (goto-char (overlay-start dot))
               (conn--push-ephemeral-mark (overlay-end dot))
               (conn-state))))

(defun conn-dots-change (&optional multi-buffer)
  (interactive "P")
  (conn--macro-dispatch
   (cond ((consp multi-buffer)
          (conn-read-matching-dot-buffers))
         (multi-buffer
          (conn-read-dot-buffers))
         (t (list (current-buffer))))
   :setup-fn (lambda (dot)
               (goto-char (overlay-start dot))
               (delete-region (point) (overlay-end dot))
               (conn--push-ephemeral-mark (point))
               (emacs-state))))

(defun conn-dots-insert (&optional multi-buffer)
  (interactive "P")
  (conn--macro-dispatch
   (cond ((consp multi-buffer)
          (conn-read-matching-dot-buffers))
         (multi-buffer
          (conn-read-dot-buffers))
         (t (list (current-buffer))))
   :setup-fn (lambda (dot)
               (goto-char (overlay-start dot))
               (emacs-state))))

(defun conn-dots-insert-after (&optional multi-buffer)
  (interactive "P")
  (conn--macro-dispatch
   (cond ((consp multi-buffer)
          (conn-read-matching-dot-buffers))
         (multi-buffer
          (conn-read-dot-buffers))
         (t (list (current-buffer))))
   :setup-fn (lambda (dot)
               (goto-char (overlay-end dot))
               (emacs-state))))

;;;;; Isearch commands

(defun conn-isearch-in-dot-p (beg end)
  (when-let ((ov (conn--dot-at-point beg)))
    (>= (overlay-end ov) end)))

(defun conn-isearch-not-in-dot-p (beg end)
  (not (conn-isearch-in-dot-p beg end)))

(defun conn-isearch-in-dot-toggle ()
  (interactive)
  (if (advice-function-member-p #'conn-isearch-in-dot-p isearch-filter-predicate)
      (advice-remove isearch-filter-predicate #'conn-isearch-in-dot-p)
    (advice-add isearch-filter-predicate :after-while #'conn-isearch-in-dot-p
                '((isearch-message-prefix . "[DOT] "))))
  (isearch-update))

(defun conn--isearch-add-dots-1 (&optional buffer)
  (with-current-buffer (or buffer (current-buffer))
    (let ((in-dots (advice-function-member-p #'conn-isearch-in-dot-p isearch-filter-predicate))
          (beg (if isearch-forward (point-min) (point-max)))
          (end (if isearch-forward (point-max) (point-min)))
          new-dots)
      (save-excursion
        (goto-char beg)
        (while (isearch-search-string isearch-string end t)
          (when (funcall isearch-filter-predicate
                         (match-beginning 0) (match-end 0))
            (push (cons (match-beginning 0) (match-end 0)) new-dots)))
        (when in-dots
          (conn--clear-dots (point-min) (point-max)))
        (apply #'conn--create-dots new-dots)))))

(defun conn-isearch-add-dots ()
  (interactive)
  (if (not multi-isearch-buffer-list)
      (conn--isearch-add-dots-1 (current-buffer))
    (dolist (buf multi-isearch-buffer-list)
      (when buf
        (conn--isearch-add-dots-1 buf))))
  (isearch-update))

(defun conn-isearch-remove-dots (&optional partial-match)
  (interactive "P")
  (save-excursion
    (dolist (dot (conn--all-overlays #'conn-dotp))
      (goto-char (overlay-start dot))
      (when (and (isearch-search-string isearch-string (overlay-end dot) t)
                 (funcall isearch-filter-predicate
                          (match-beginning 0) (match-end 0))
                 (or (not partial-match)
                     (and (= (match-beginning 0) (overlay-start dot))
                          (= (match-end 0) (overlay-end dot)))))
        (conn--delete-dot dot)))))

(defun conn-isearch-split-dots ()
  "Split region from START to END into dots on REGEXP."
  (interactive)
  (let (dots)
    (save-excursion
      (dolist (dot (conn--all-overlays #'conn-dotp))
        (goto-char (overlay-start dot))
        (push (point) dots)
        (while (isearch-search-string isearch-string (overlay-end dot) t)
          (when (funcall isearch-filter-predicate
                         (match-beginning 0) (match-end 0))
            (push (match-beginning 0) dots)
            (push (match-end 0) dots)))
        (push (overlay-end dot) dots)))
    (conn--clear-dots)
    (cl-loop for (beg end) on dots by #'cddr
             when (/= beg end)
             do (conn--create-dots (cons beg end)))))

(defun conn-isearch-dot-change ()
  (interactive)
  (with-isearch-suspended
   (conn-dots-change)))

(defun conn-isearch-dot-insert ()
  (interactive)
  (with-isearch-suspended
   (conn-dots-insert)))

(defun conn-isearch-dot-insert-after ()
  (interactive)
  (with-isearch-suspended
   (conn-dots-insert-after)))

(defun conn-isearch-dot-command ()
  (interactive)
  (with-isearch-suspended
   (conn-dots-command)))

(defun conn-isearch-done-and-clear-dots ()
  (interactive)
  (conn--clear-dots)
  (isearch-done))

(defun conn-isearch-dot-jump-to-register ()
  (interactive)
  (with-isearch-suspended
   (jump-to-register
    (register-read-with-preview "Dot macro from register: "))))

(defun conn-isearch-delete-inside-dots ()
  (interactive)
  (with-isearch-suspended
   (conn--for-each-dot (lambda (ov)
                         (delete-region (overlay-start ov)
                                        (overlay-end ov)))
                       '>)))

;;;;; Commands

(defun conn-last-dot-macro-to-register (register)
  "Set REGISTER to last dot macro."
  (interactive (list (register-read-with-preview "register: ")))
  (set-register register (get-register conn-last-dot-macro-register)))

(defun conn-remove-dot-at-point (point)
  "Remove dot at POINT."
  (interactive (list (point)))
  (save-mark-and-excursion
    (if-let ((dot (conn--dot-at-point point)))
        (progn
          (conn--delete-dot dot)
          (when (called-interactively-p 'interactive)
            (message "Dot removed")))
      (when (called-interactively-p 'interactive)
        (message "No dot at point")))))

(defun conn-remove-all-dots (&optional multi-buffer)
  "Remove all dots.

With a plain prefix argument (\\[universal-argument]), prompt for a
regular expression and remove all dots in all buffers whose name
matches the expression.

With a numerical prefix argument read buffers using `completing-read'."
  (interactive "P")
  (cond ((consp multi-buffer)
         (conn--clear-dots-in-buffers (conn-read-matching-dot-buffers)))
        (multi-buffer
         (conn--clear-dots-in-buffers (conn-read-dot-buffers)))
        (t (conn--clear-dots
            (conn--beginning-of-region-or-restriction)
            (conn--end-of-region-or-restriction))))
  (when (called-interactively-p 'interactive)
    (message "Dots removed")))

(defun conn-dot-undo ()
  "Undo last dot change."
  (interactive)
  (unless conn--dot-undo-ring
    (user-error "Dot undo ring is empty"))
  (let ((this-undo (car (push (pop conn--dot-undo-ring) conn--dot-undone)))
        (conn--dot-undoing t))
    (dolist (action (sort this-undo
                          ;; We need to undo creation first to avoid possible overlaps
                          (lambda (a1 _a2)
                            (eq (car a1) 'create))))
      (pcase action
        (`(create ,beg . ,_end)
         (let ((dot (or (conn--dot-at-point beg)
                        (error "Dot undo ring corrupted"))))
           (conn--delete-dot dot)))
        (`(delete ,beg . ,end)
         (conn--create-dots (cons beg end)))
        (`(move (,to-beg . ,_to-end) . (,from-beg . ,from-end))
         (let ((dot (or (conn--dot-at-point to-beg)
                        (error "Dot undo ring corrupted"))))
           (conn--move-dot dot from-beg from-end)))))
    (when (called-interactively-p 'interactive)
      (message "Dots undone"))))

(defun conn-dot-redo ()
  "Redo last dot change."
  (interactive)
  (unless conn--dot-undone
    (user-error "No further redo information"))
  (let ((this-redo (car (push (pop conn--dot-undone) conn--dot-undo-ring)))
        (conn--dot-undoing t))
    (dolist (action (sort this-redo
                          ;; And here we need to redo deletion first
                          (lambda (a1 _a2)
                            (eq (car a1) 'delete))))
      (pcase action
        (`(create ,beg . ,end)
         (conn--create-dots (cons beg end)))
        (`(delete ,beg . ,_end)
         (let ((dot (or (conn--dot-at-point beg)
                        (error "Dot undo ring corrupted"))))
           (conn--delete-dot dot)))
        (`(move (,to-beg . ,to-end) . (,from-beg . ,_from-end))
         (let ((dot (or (conn--dot-at-point from-beg)
                        (error "Dot undo ring corrupted"))))
           (conn--move-dot dot to-beg to-end)))))
    (when (called-interactively-p 'interactive)
      (message "Dots redone"))))

(defun conn-apply-on-dots (function &optional activate-mark)
  "Call FUNCTION on each dot. `point' and `mark' are set
to end and start of dot respectively before function is called."
  (interactive
   (list (eval (read--expression "Expression: "))
         current-prefix-arg))
  (save-mark-and-excursion
    (conn--for-each-dot
     (lambda (dot)
       (goto-char (overlay-end dot))
       (push-mark (overlay-start dot) t activate-mark)
       (funcall function)))))

(defun conn-dots-reverse ()
  "Reverse dots."
  (interactive)
  (let ((dots (conn--sorted-overlays #'conn-dotp)))
    (conn--replace-each-dot (mapcar #'conn--dot-string (reverse dots)) dots)))

(defun conn-first-dot (&optional start)
  "Go to the end of the first dot in buffer.
If start is non-nil go to the start of last dot instead."
  (interactive "P")
  (when-let ((dot (save-excursion
                    (goto-char (point-min))
                    (conn--next-dot-1))))
    (if start
        (progn
          (goto-char (overlay-start dot))
          (conn--push-ephemeral-mark (overlay-end dot)))
      (goto-char (overlay-end dot))
      (conn--push-ephemeral-mark (overlay-start dot)))))

(defun conn-last-dot (&optional start)
  "Go to the end of the last dot in buffer.
If start is non-nil go to the start of last do instead."
  (interactive "P")
  (when-let ((dot (save-excursion
                    (goto-char (point-max))
                    (conn--previous-dot-1)
                    (when-let ((ov (conn--dot-at-point (point))))
                      (goto-char (overlay-end ov))))))
    (if start
        (progn
          (goto-char (overlay-start dot))
          (conn--push-ephemeral-mark (overlay-end dot)))
      (goto-char (overlay-end dot))
      (conn--push-ephemeral-mark (overlay-start dot)))))

(defun conn-remove-dot-backward (arg)
  "Remove nearest dot within the range `point-min' to `point'."
  (interactive "p")
  (let ((dot (or (conn--dot-at-point (point))
                 (conn--previous-dot-1))))
    (while (and (> arg 0) dot)
      (conn--delete-dot dot)
      (setq dot (or (conn--dot-at-point (point))
                    (conn--previous-dot-1))
            arg (1- arg)))
    (when dot
      (conn--push-ephemeral-mark (overlay-start dot))))
  (when (called-interactively-p 'interactive)
    (message "Region removed backward")))

(defun conn-remove-dot-forward (arg)
  "Remove nearest dot within the range `point' to `point-max'."
  (interactive "p")
  (let ((dot (or (conn--dot-at-point (point))
                 (conn--next-dot-1))))
    (while (and (> arg 0) dot)
      (conn--delete-dot dot)
      (setq dot (or (conn--dot-at-point (point))
                    (conn--next-dot-1))
            arg (1- arg)))
    (when dot
      (conn--push-ephemeral-mark (overlay-start dot))))
  (when (called-interactively-p 'interactive)
    (message "Region removed forward")))

(defun conn-dot-region (bounds)
  "Dot current region."
  (interactive (list (conn--region-bounds)))
  (apply #'conn--create-dots bounds)
  (deactivate-mark))

(defun conn-dot-region-forward (start end &optional arg)
  "Dot region and `search-foward' for string matching region.
If ARG is non-nil repeat ARG times.
If region is already a dot `search-forward', dot, and `search-forward' again."
  (interactive (list (conn--region-beginning)
                     (conn--region-end)
                     (prefix-numeric-value current-prefix-arg)))
  (let ((str (buffer-substring-no-properties start end)))
    (unless (= (point) end)
      (exchange-point-and-mark t))
    (let ((dot (conn--dot-at-point (point))))
      (unless (and dot
                   (= start (overlay-start dot))
                   (= end (overlay-end dot)))
        (when dot (conn--delete-dot dot))
        (conn--create-dots (cons start end))
        (cl-decf arg)))
    (search-forward str)
    (when (numberp arg)
      (dotimes (_ (1- arg))
        (conn--create-dots (cons (match-beginning 0) (match-end 0)))
        (search-forward str)))
    (conn--push-ephemeral-mark (match-beginning 0)))
  (when (called-interactively-p 'interactive)
    (message "Region dotted forward")))

(defun conn-dot-region-backward (start end &optional arg)
  "Dot region and `search-backward' for string matching region.
If ARG is non-nil repeat ARG times.
If region is already a dot `search-backward', dot, and `search-backward' again."
  (interactive (list (conn--region-beginning)
                     (conn--region-end)
                     (prefix-numeric-value current-prefix-arg)))
  (let ((str (buffer-substring-no-properties start end)))
    (unless (= (point) start)
      (exchange-point-and-mark t))
    (let ((dot (conn--dot-at-point (point))))
      (unless (and dot
                   (= start (overlay-start dot))
                   (= end (overlay-end dot)))
        (when dot (conn--delete-dot dot))
        (conn--create-dots (cons start end))
        (cl-decf arg)))
    (search-backward str)
    (when (numberp arg)
      (dotimes (_ arg)
        (conn--create-dots (cons (match-beginning 0) (match-end 0)))
        (search-backward str)))
    (conn--push-ephemeral-mark (match-end 0)))
  (when (called-interactively-p 'interactive)
    (message "Region dotted backward")))

(defun conn-dot-skip-forward (start end &optional arg)
  "`search-forward', skipping this region."
  (interactive (list (conn--region-beginning)
                     (conn--region-end)
                     current-prefix-arg))
  (let ((str (buffer-substring-no-properties start end)))
    (unless (= (point) end)
      (exchange-point-and-mark t))
    (dotimes (_ (or (and (numberp arg) arg) 1))
      (search-forward str))
    (conn--push-ephemeral-mark (match-beginning 0)))
  (when (called-interactively-p 'interactive)
    (message "Region skipped forward")))

(defun conn-dot-skip-backward (start end &optional arg)
  "`search-backward', skipping this region."
  (interactive (list (conn--region-beginning)
                     (conn--region-end)
                     current-prefix-arg))
  (with-demoted-errors "%s"
    (let ((str (buffer-substring-no-properties start end)))
      (unless (= (point) start)
        (exchange-point-and-mark t))
      (dotimes (_ (or (and (numberp arg) arg) 1))
        (search-backward str))
      (conn--push-ephemeral-mark (match-end 0))))
  (when (called-interactively-p 'interactive)
    (message "Region skipped backward")))

(defun conn-dot-thing (thing)
  "Transpose things at point and mark."
  (interactive (list (intern
                      (completing-read
                       (format "Thing: ")
                       (conn--things 'conn--defined-thing-p) nil nil nil
                       'conn-thing-history))))
  (conn--create-dots (bounds-of-thing-at-point thing)))

(defun conn-add-dots-matching-string (string &optional start end in-dots)
  "Dot all occurances of STRING in region from START to END.
If IN-DOTS is non-nil only dot occurances in dots.

When region is active operates within `region-bounds', otherwise operates
between `point-min' and `point-max'."
  (interactive (list (read-string "String: ")
                     (conn--beginning-of-region-or-restriction)
                     (conn--end-of-region-or-restriction)
                     current-prefix-arg))
  (setq start (or start (point-min))
        end (or end (point-max)))
  (let (new-dots)
    (save-excursion
      (goto-char start)
      (while (search-forward string end t)
        (cond
         ((not in-dots)
          (conn--create-dots (cons (match-beginning 0) (match-end 0))))
         ((conn-isearch-in-dot-p (match-beginning 0) (match-end 0))
          (push (cons (match-beginning 0) (match-end 0)) new-dots))))
      (when in-dots
        (conn--clear-dots start end)
        (apply #'conn--create-dots new-dots)))))

(defun conn-add-dots-matching-regexp (regexp &optional start end in-dots)
  "Dot things matching REGEXP in region from START to END.
If IN-DOTS is non-nil only dot thing withing dots in
region from START to END.

When region is active operates within `region-bounds', otherwise operates
between `point-min' and `point-max'."
  (interactive (list (read-regexp "Regexp: ")
                     (conn--beginning-of-region-or-restriction)
                     (conn--end-of-region-or-restriction)
                     current-prefix-arg))
  (setq start (or start (point-min))
        end (or end (point-max)))
  (let (new-dots)
    (save-excursion
      (goto-char start)
      (while (re-search-forward regexp end t)
        (cond
         ((not in-dots)
          (conn--create-dots (cons (match-beginning 0) (match-end 0))))
         ((conn-isearch-in-dot-p (match-beginning 0) (match-end 0))
          (push (cons (match-beginning 0) (match-end 0)) new-dots))))
      (when in-dots
        (conn--clear-dots start end)
        (apply #'conn--create-dots new-dots)))))

(defun conn-add-dots-matching-region (start end &optional in-dots)
  "Dot all occurances of string within region from START to END.
If IN-DOTS is non-nil only dot occurances in dots.

When called interactively uses point and mark."
  (interactive (list (conn--region-beginning)
                     (conn--region-end)
                     current-prefix-arg))
  (conn-add-dots-matching-string (buffer-substring-no-properties start end)
                                 nil nil in-dots))

(defun conn-comment-or-uncomment-dots ()
  "`comment-or-uncomment-region' each dot."
  (interactive)
  (conn--for-each-dot
   (lambda (o)
     (comment-or-uncomment-region (overlay-start o)
                                  (overlay-end o)))))

(defun conn-dot-lines (start end)
  "Dot each line in region from START to END.

When called START is `region-beginning' and END is `region-end'."
  (interactive (list (conn--region-beginning)
                     (conn--region-end)))
  (save-excursion
    (goto-char start)
    (conn--create-dots (cons (line-beginning-position)
                             (line-end-position)))
    (while (> end (progn (forward-line) (point)))
      (conn--create-dots (cons (line-beginning-position)
                               (line-end-position))))))

(defun conn-remove-dots-outside (start end)
  "Remove all dots outside region from START to END.

When called interactively operates within `region-bounds'."
  (interactive (list (conn--region-beginning) (conn--region-end)))
  (conn--for-each-dot #'conn--delete-dot nil (point-min) start)
  (conn--for-each-dot #'conn--delete-dot nil end (point-max)))

(defun conn-split-region-on-regexp (regexp start end)
  "Split region from START to END into dots on REGEXP.

When region is active operates within `region-bounds', otherwise operates
between `point-min' and `point-max'."
  (interactive (list (read-regexp "Regexp" nil)
                     (conn--beginning-of-region-or-restriction)
                     (conn--end-of-region-or-restriction)))
  (let (dots
        (search-invisible 'open))
    (save-excursion
      (goto-char start)
      (push (point) dots)
      (while (re-search-forward regexp end t)
        (push (match-beginning 0) dots)
        (push (match-end 0) dots)
        (when (= (match-beginning 0) (match-end 0))
          (forward-char)))
      (push end dots))
    (conn--clear-dots start end)
    (cl-loop for (beg end) on dots by #'cddr
             do (conn--create-dots (cons beg end)))))

(defun conn-split-dots-on-regexp (regexp start end)
  "Split all dots in region START to END on regexp.

When region is active operates within `region-bounds', otherwise operates
between `point-min' and `point-max'."
  (interactive (list (read-regexp "Regexp" "[[:blank:]]")
                     (conn--beginning-of-region-or-restriction)
                     (conn--end-of-region-or-restriction)))
  (conn--for-each-dot
   (lambda (dot)
     (let ((start (overlay-start dot))
           (end (overlay-end dot)))
       (conn--delete-dot dot)
       (conn-split-region-on-regexp regexp start end)))
   nil start end))

(defun conn-split-dots-on-newline (start end)
  "Split dots in region START to END on newlines.

When region is active operates within `region-bounds', otherwise operates
between `point-min' and `point-max'."
  (interactive (list (conn--beginning-of-region-or-restriction)
                     (conn--end-of-region-or-restriction)))
  (conn-split-dots-on-regexp "\n" start end))

(defun conn-swizzle (control-string)
  "Swizzle dots.

 Number of dots must be divisible by control string length."
  (interactive "MSwizzle: ")
  (let* ((chars (mapcar (lambda (n) (- n ?0)) control-string))
         (n (length control-string))
         (dots (conn--sorted-overlays #'conn-dotp)))
    (cond ((/= 0 (mod (length dots) n))
           (user-error "Invalid swizzle length"))
          ((>= (apply #'max chars) n)
           (user-error "Invalid swizzle control char")))
    (let ((set (seq-subseq dots 0 n))
          (rest (nthcdr n dots)))
      (while set
        (conn--replace-each-dot (mapcar (lambda (i)
                                          (conn--dot-string (nth i set)))
                                        chars)
                                set)
        (setq set (seq-subseq rest 0 n)
              rest (nthcdr n rest))))))

(defun conn--next-dot-1 ()
  "Perform one iteration for `conn-next-dot-end'."
  (let* ((pt (next-overlay-change (point)))
         (ov (conn--dot-at-point pt)))
    (while (and (or (not ov)
                    (/= pt (overlay-end ov)))
                (/= pt (point-max)))
      (setq pt (next-overlay-change pt)
            ov (conn--dot-at-point pt)))
    (if ov
        (progn
          (goto-char pt)
          ov)
      (message "No more dots")
      nil)))

(defun conn--previous-dot-1 ()
  "Perform one iteration for `conn-previous-dot-end'."
  (interactive)
  (let* ((pt (previous-overlay-change (point)))
         (ov (conn--dot-at-point pt)))
    (while (and (or (not ov)
                    (/= pt (overlay-start ov)))
                (/= pt (point-min)))
      (setq pt (previous-overlay-change pt)
            ov (conn--dot-at-point pt)))
    (if ov
        (progn (goto-char pt) ov)
      (message "No more dots")
      nil)))

(defun conn-next-dot (arg)
  (interactive "p")
  (cond ((> arg 0)
         (dotimes (_ arg)
           (conn--next-dot-1)))
        ((< arg 0)
         (dotimes (_ (abs arg))
           (conn--previous-dot-1)))))
(put 'dot 'forward-op 'conn-next-dot)

(defun conn-previous-dot (arg)
  (interactive "p")
  (conn-next-dot (- arg)))

(defun conn-rotate-dots (N)
  "Rotate dots. With prefix arg N rotate by N."
  (interactive "p")
  (let ((dots (conn--sorted-overlays #'conn-dotp)))
    (conn--replace-each-dot (conn--rotate N (mapcar #'conn--dot-string dots))
                            dots)))

(defun conn-rotate-dots-backward (N)
  "Rotate dots. With prefix arg N rotate by N."
  (interactive "p")
  (conn-rotate-dots (- N)))

(defun conn-kill-to-dots (start end)
  "Kill region from START to END and insert region to each dot.
When called interactively START and END default to point and mark."
  (interactive (list (conn--region-beginning)
                     (conn--region-end)))
  (let ((str (buffer-substring start end)))
    (delete-region start end)
    (conn--for-each-dot
     (lambda (dot)
       (save-mark-and-excursion
         (goto-char (overlay-start dot))
         (insert str))))))

(defun conn-dot-point (point)
  "Insert dot at point."
  (interactive (list (point)))
  (conn--create-dots (cons point (1+ point))))

(defun conn-dot-at-click (event)
  "Insert dot at mouse click."
  (interactive "e")
  (mouse-minibuffer-check event)
  (let* ((start-posn (event-start event))
         (start-point (posn-point start-posn))
         (start-window (posn-window start-posn)))
    (with-current-buffer (window-buffer start-window)
      (conn-dot-point start-point)
      (goto-char start-point))))

(defun conn-dot-text-property (start end &optional in-dots)
  "Dot each region between START and END with text property PROP equal to VAL.

When region is active operates within `region-bounds', otherwise operates
between `point-min' and `point-max'."
  (interactive (list (conn--beginning-of-region-or-restriction)
                     (conn--end-of-region-or-restriction)
                     current-prefix-arg))
  (let* ((prop (intern (completing-read
                        "Property: "
                        (cl-loop for prop in (text-properties-at (point))
                                 by #'cddr
                                 collect prop)
                        nil t)))
         (vals (mapcar (lambda (s) (cons (message "%s" s) s))
                       (ensure-list (get-text-property (point) prop))))
         (val (alist-get (completing-read "Value: " vals) vals
                         nil nil #'string=))
         new-dots)
    (save-excursion
      (with-restriction
        start end
        (goto-char (point-min))
        (let (match)
          (while (setq match (text-property-search-forward prop val t))
            (cond ((null in-dots)
                   (conn--create-dots (cons (prop-match-beginning match)
                                            (prop-match-end match))))
                  ((conn-isearch-in-dot-p (prop-match-beginning match)
                                          (prop-match-end match))
                   (push (cons (prop-match-beginning match)
                               (prop-match-end match))
                         new-dots)))))
        (when in-dots
          (conn--clear-dots start end)
          (apply #'conn--create-dots new-dots))))))

(defun conn-dot-trim-regexp (regexp start end)
  "Trim whitespace from ends of all dots.

When region is active operates within `region-bounds', otherwise operates
between `point-min' and `point-max'."
  (interactive (list (read-regexp "Regexp" "[[:blank:]]+")
                     (conn--beginning-of-region-or-restriction)
                     (conn--end-of-region-or-restriction)))
  (conn--for-each-dot
   (lambda (dot)
     (let ((start (overlay-start dot))
           (end (overlay-end dot)))
       (goto-char start)
       (when (looking-at regexp)
         (setq start (match-end 0)))
       (goto-char end)
       (when (looking-back regexp start t)
         (setq end (match-beginning 0)))
       (if (>= start end)
           (conn--delete-dot dot)
         (move-overlay dot start end))))
   '> start end))

(defun conn-delete-all-dot-regions ()
  "Delete text in all dots."
  (interactive)
  (conn--for-each-dot
   (lambda (dot)
     (delete-region (overlay-start dot) (overlay-end dot))
     (conn--delete-dot dot))))

(defun conn-query-remove-dots ()
  "Prompt to keep each dot."
  (interactive)
  (save-excursion
    (conn--for-each-dot
     (lambda (dot)
       (goto-char (overlay-start dot))
       (unless (y-or-n-p "Keep this dot?")
         (conn--delete-dot dot)))
     #'<)))

(defun conn-remove-dots-after (point)
  "Clear all dots after POINT."
  (interactive (list (point)))
  (conn--clear-dots point (point-max))
  (when (called-interactively-p 'interactive)
    (message "Dots after point removed")))

(defun conn-remove-dots-before (point)
  "Clear all dots before POINT."
  (interactive (list (point)))
  (conn--clear-dots (point-min) point)
  (when (called-interactively-p 'interactive)
    (message "Dots before point removed")))

(defun conn-yank-region-to-dots (&optional prepend)
  "Copy region between point and mark to each dot.
If prepend is non-nil prepend text instead of appending it."
  (interactive "P")
  (let ((region (buffer-substring (conn--region-beginning) (conn--region-end))))
    (save-excursion
      (conn--for-each-dot
       (lambda (dot)
         (let ((pt (if prepend
                       (overlay-start dot)
                     (overlay-end dot))))
           (goto-char pt)
           (insert region)))))))

(defun conn-yank-dots-to-region (seperator)
  "Copy text within each to region between point and mark."
  (interactive (list (read-string "Seperator: " "\n")))
  (let ((first t))
    (delete-region (conn--region-beginning) (conn--region-end))
    (conn--for-each-dot
     (lambda (dot)
       (if first
           (setq first nil)
         (insert seperator))
       (insert (buffer-substring (overlay-start dot)
                                 (overlay-end dot)))
       (delete-region (overlay-start dot)
                      (overlay-end dot))
       (conn--delete-dot dot)))))

(defun conn-dot-all-things-in-region (thing)
  "Dot all THINGs in region.

THING is something with a forward-op as defined by thingatpt."
  (interactive
   (let ((things (conn--things 'conn--movement-thing-p)))
     (list (intern (completing-read "Thing: " things nil t nil
                                    'conn-thing-history)))))
  (save-excursion
    (with-restriction
      (conn--region-beginning) (conn--region-end)
      (goto-char (point-min))
      (forward-thing thing)
      (conn--create-dots (bounds-of-thing-at-point thing))
      (while (and (/= (point) (point-max))
                  (/= (point) (progn
                                (forward-thing thing)
                                (point))))
        (conn--create-dots (bounds-of-thing-at-point thing))))))

(provide 'conn-dots)
;;; conn-dots.el ends here
